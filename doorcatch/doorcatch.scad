coreHeight = 20.0;
coreRadius =  9.5;
holeRadius =  2.0;
sinkRadius =  4.5;
sinkDepth  =  2.0;
flangeRadius = 11.0;

module core()
{
  cylinder(h=coreHeight, r=coreRadius, center=true, $fa=1);
  cylinder(h=coreHeight, r1=flangeRadius, r2=0,center=true);
}


module hole()
{
  cylinder(h=coreHeight+1, r=holeRadius, center=true);
}

module countersink()
{
  translate([0,0,coreHeight/2-sinkDepth/2]) {
    cylinder(h=sinkDepth, r1=holeRadius, r2=sinkRadius, center=true);
  }
}

difference()
{
  core();
  hole();
  countersink();
}