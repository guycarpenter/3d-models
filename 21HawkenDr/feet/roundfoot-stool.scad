/* for wine rack */
innerR = 19.8/2;  // first set made at 19.1 were very very tight.  Have not yet printed these at 19.8
outerR1 = innerR+3.0;
outerR2 = innerR+2.0;
footThickness = 3.0;
height = 10.0;
small = 0.2;
$fn = 64;

// for an angled leg, how much does the high edge sit above the floor
// if you don't use an angled foot?
lift = 2.0;
slopeAngle = asin(lift/outerR2);

airHoleR = 1.0;

module hole()
{
  cylinder(r=innerR, h=height+small);
  translate([0,0,-footThickness-lift-small]) {
    cylinder(r=airHoleR, h=footThickness+lift+small*2);
  }
}

module cap()
{
  translate([0,0,-footThickness-lift-small])
  {
    cylinder(r1=outerR1, r2=outerR2, h=height+footThickness+lift+small);
  }
}

module slopedBottom()
{
  { //translate([0,0,-footThickness]) {
    rotate([1,0,0], 15) {
      translate([0,0,-50]) {
        cube([100,100,100], center=true);
      }
    }
  }
}

module foot() {
  difference() 
  {
    rotate(a=[slopeAngle,0,0]) {
      difference()
      {
        cap();
        hole();
      }
    }
    translate([0,0,-50-footThickness]) {
      cube([100,100,100], center=true);
    }
  }
}

module setof4() {
  translate([0,0,0]) foot();
  translate([30,0,0]) foot();
  translate([0,30,0]) foot();
  translate([30,30,0]) foot();
}


setof4();
//foot();
