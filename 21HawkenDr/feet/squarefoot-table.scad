innerX = 22.0;  // 22.4 is too loose 22.9 is too loose
innerY = 22.0;
outerX = 25.4;
outerY = 25.4;
outerR = 2;
innerR = 4;
wallThickness = 2.0;
voidR = innerR-wallThickness;

voidX = innerX - wallThickness*2;
voidY = innerY - wallThickness*2;
footThickness = 2.0;
height = 5.0;
small = 0.1;
$fn = 32;
airHoleR = 1.0;

module boxCornerNegative(centre, size, rotation)
{
  translate(centre) {
    rotate([0,0,rotation])
    {
      difference()
      {
        translate([-small,-small,-small/2]) {
          cube([size[0]+small,size[1]+small,size[2]+small]);
        }
        translate([size[0],size[1],-small]) {
          cylinder(h=size[2]+small*2,r=size[0], center=false);
        }
      }
    }
  }
}

module roundedBox(size, radii)
{
  difference()
  {
    cube(size);
    boxCornerNegative([0,0,0],[radii[0],radii[0],size[2]],0);
    boxCornerNegative([0,size[1],0],[radii[1],radii[1],size[2]],270);
    boxCornerNegative([size[0],0,0],[radii[2],radii[2],size[2]],90);
    boxCornerNegative([size[0],size[1],0],[radii[3],radii[3],size[2]],180);
  }
}

module hole()
{
  translate([0,0,-footThickness-small]) {
    cylinder(r=airHoleR, h=footThickness+small*3);
  }
  translate([-voidX/2,-voidY/2,small]) {
    roundedBox([voidX,voidY,height], [voidR,voidR,voidR,voidR]);
  }
}

module cap()
{
  translate([-outerX/2,-outerY/2,-footThickness]) {
    roundedBox([outerX,outerY,footThickness], [outerR,outerR,outerR,outerR]);
  }
  translate([-innerX/2,-innerY/2,0]) {
    roundedBox([innerX,innerY, height], [innerR,innerR,innerR,innerR]);
  }
  
}

module foot()
{
  difference()
  {
    cap();
    hole();
  }
}

module setof4()
{
  dx = outerX + 5;
  dy = outerY + 5;
  translate([0,0,0]) foot();
  translate([dx,0,0]) foot();
  translate([0,dy,0]) foot();
  translate([dx,dy,0]) foot();
}

//setof4();

foot();
