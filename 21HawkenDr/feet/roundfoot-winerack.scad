/* for wine rack */
innerR = 8.8/2;
outerR1 = innerR+3.0;
outerR2 = innerR+1.0;
footThickness = 1.0;
height = 10.0;
small = 0.1;
$fn = 32;

airHoleR = 1.0;

module hole()
{
  cylinder(r=innerR, h=height+small);
  translate([0,0,-footThickness-small]) {
    cylinder(r=airHoleR, h=footThickness+small*2);
  }
}

module cap()
{
  translate([0,0,-footThickness])
  {
    cylinder(r1=outerR1, r2=outerR2, h=height+footThickness);
  }
}

module foot() {
  difference()
  {
    cap();
    hole();
  }
}

module setof4() {
  translate([0,0,0]) foot();
  translate([20,0,0]) foot();
  translate([0,20,0]) foot();
  translate([20,20,0]) foot();
}


setof4();
