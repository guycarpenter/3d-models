$fn = 64;
small = 0.1;

frontOuterR = 42/2;
frontInnerR = frontOuterR-5;
frontHeight = 5;
frontRecess = 2;

module front()
{
  difference()
  {
    cylinder(r=frontOuterR, h=frontHeight);
    translate([0,0,frontHeight-frontRecess]) {
      cylinder(r=frontInnerR, h=frontRecess+small);
    }
  }
}

backOuterR = 28/2;
backInnerR = backOuterR - 3;
backHeight = frontHeight + 2.8;
backRecess = 4.0;

module back()
{
  difference()
  {
    cylinder(r=backOuterR, h=backHeight);
    translate([0,0,backHeight-backRecess]) {
      cylinder(r=backInnerR, h=backRecess+small);
    }
  }
}

module pair()
{
  translate([frontOuterR,0,0]) front();
  translate([-backOuterR - 2,0,0]) back();
}

pair();
translate([0, frontOuterR*2 +2]) pair();
