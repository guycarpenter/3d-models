innerX = 44;
innerY = 20;
outerX = 52;
outerY = 27;
wallThickness = 2.0;
voidX = innerX - wallThickness*2;
voidY = innerY - wallThickness*2;
footThickness = 1.0;
height = 5.0;
small = 0.1;
$fn = 32;
airHoleR = 1.0;

module boxCornerNegative(centre, size, rotation)
{
  translate(centre) {
    rotate([0,0,rotation])
    {
      difference()
      {
        translate([-small,-small,-small/2]) {
          cube([size[0]+small,size[1]+small,size[2]+small]);
        }
        translate([size[0],size[1],-small]) {
          cylinder(h=size[2]+small*2,r=size[0], center=false);
        }
      }
    }
  }
}

module roundedBox(size, radii)
{
  difference()
  {
    cube(size);
    boxCornerNegative([0,0,0],[radii[0],radii[0],size[2]],0);
    boxCornerNegative([0,size[1],0],[radii[1],radii[1],size[2]],270);
    boxCornerNegative([size[0],0,0],[radii[2],radii[2],size[2]],90);
    boxCornerNegative([size[0],size[1],0],[radii[3],radii[3],size[2]],180);
  }
}

module hole()
{
  translate([0,0,-footThickness-small]) {
    cylinder(r=airHoleR, h=footThickness+small*2);
  }
  translate([0,0,+height/2]) {
    cube([voidX,voidY,height+small], center=true);
  }
}

module cap()
{
  translate([-outerX/2,-outerY/2,-footThickness]) {
    roundedBox([outerX,outerY,footThickness], [2,2,2,2]);
  }
  translate([0,0,+height/2]) {
    cube([innerX,innerY, height], center=true);
  }
  
}

module foot()
{
  difference()
  {
    cap();
    hole();
  }
}

module setof4()
{
  translate([0,0,0]) foot();
  translate([55,0,0]) foot();
  translate([0,30,0]) foot();
  translate([55,30,0]) foot();
}

setof4();
