small = 0.1;
large = 100;
$fn = 32;

thickness = 6.0;
width = 10.0;
wallThickness = 11.5;
wallInset = 4.0;
drop = 100.0;
hookOverhang = 20;

hookInnerR = wallThickness/2;
hookOuterR = hookInnerR + thickness;

wireR = 4.4/2;
wireY = -100;
wireX = wallThickness/2 + 27 - wallInset - wireR;

mountDx = 2;
mountDy = 2;
mountThickness = 5;
mountR = wireR + mountThickness;
mountJoinR = 4;

footHeight = 3;
footDrop = drop-footHeight+mountDy;

module Hook()
{
  translate() {
    difference() {
      cylinder(r=hookOuterR, h=width);
      translate([0,0,-small]) {
        cylinder(r=hookInnerR, h=width+small*2);
      }
      translate([-large/2,-large-small,-large/2]) {
        cube([large,large,large]);
      }
    }
    translate([-wallThickness/2-thickness/2,-hookOverhang,0]) {
      cylinder(r=thickness/2, h=width);
    }
    translate([-wallThickness/2-thickness,-hookOverhang,0]) {
      cube([thickness, hookOverhang, width]);
    }
  }
}

module Drop()
{
  translate([wallThickness/2,-drop,0]) {
    cube([thickness, drop, width]);
  }
}

module Foot()
{
  translate([wallThickness/2-wallInset,-footDrop,0]) {
    cube([wallInset,footHeight,width]);
  }
}

module Mount()
{
  translate([wallThickness/2,-drop-mountDy,0]) {
    cube([thickness,mountDy,width]);
  }
  difference() {
    hull() {
      translate([wireX+mountDx,wireY-mountDy,0]) {
        cylinder(r=mountR, h=width);
      }

      // bottom of drop
      translate([wallThickness/2+mountR,-drop-mountDy,0]) {
       cylinder(r=mountR, h=width);
      }
    }
    Wire();
    translate([wireX-wireR,-drop,-small]) {
      cube([wireR*2,wireR+mountThickness+small,width+small*2]);
    }
  }

  translate([-small+wallThickness/2+thickness,-drop-small+mountThickness,0]) {
    difference() {
      cube([mountJoinR+small,mountJoinR+small, width]);
      translate([mountJoinR+small,mountJoinR+small,-small]) {
        cylinder(r=mountJoinR,h=width+small*2);
      }
    }
  }
}

module Wire()
{
  translate([wireX,wireY,-small]) {
    cylinder(r=wireR, h=width+small*2);
  }
}


Hook();
Drop();
Foot();
Mount();
%Wire();
