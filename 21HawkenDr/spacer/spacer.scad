pegR1 = 5/2;
pegR2 = 4/2;
pegH = 3;
outerR1 = 13.0/2;
outerR2 = 10.0/2;
height = 10.0;
small = 0.1;
$fn = 32;

module peg()
{
  translate([0,0,height-small]) {
    cylinder(r1=pegR1, r2=pegR2, h=pegH+small);
  }
}

module body()
{
  peg();
  cylinder(r1=outerR1, r2=outerR2, h=height);
}

body();
