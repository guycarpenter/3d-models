small = 0.1;

height = 22;
mountHeight = height;
mountThickness = 5;
mountLength = 50;

strikerHeight = height + 0;
strikerThickness = 6;
strikerLength = 80;
strikerAngle = 15;
strikerBackset = 33 - strikerThickness;

holeR = 5/2;
holeX1 = 9;
holeZ1 = mountHeight/2+0.5;
holeX2 = 37;
holeZ2 = mountHeight/2+0.5;
hole2DZ = 2;

joinThickness = 10;

$fn = 24;


module striker()
{
  rotate(a=[0,0,strikerAngle]) {
    translate([-strikerLength,strikerBackset,mountHeight-strikerHeight]) {
      cube([strikerLength, strikerThickness, strikerHeight]);
    }
  }
}

module mount()
{
  cube([mountLength, mountThickness, mountHeight]);
}

module holes()
{
  translate([holeX1, mountThickness+small, holeZ1]) {
    rotate(a=[90,0,0]) {
      cylinder(r=holeR, h=mountThickness+small*2);
    }
  }
  translate([holeX2, mountThickness+small, holeZ2]) {
    rotate(a=[90,0,0]) {
      hull() {
        translate([0,hole2DZ/2,0]) {
          cylinder(r=holeR, h=mountThickness+small*2);
        }
        translate([0,-hole2DZ/2,0]) {
          cylinder(r=holeR, h=mountThickness+small*2);
        }
      }
    }
  }
}

joinR = 3;
joinThicken = 6;
module join()
{
  translate([0,0,mountHeight-strikerHeight]) {
   hull() {
    translate([-joinThickness+joinR,joinR,0]) {
      cylinder(h=strikerHeight, r=joinR);
    }

    translate([-joinR,strikerBackset + strikerThickness - joinR,0]) {
      cylinder(h=strikerHeight, r=joinR);
    }

    translate([-joinThickness+joinR,0,0]) {
      cube([joinThickness-joinR, mountThickness, strikerHeight]);
    }


    rotate(a=[0,0,strikerAngle]) {
      translate([-joinThicken,strikerBackset + strikerThickness - joinR,0]) {
        cylinder(h=strikerHeight, r=joinR);
      }
    }
   }

   rotate(a=[0,0,strikerAngle]) {
     translate([-joinThicken-filletR-joinR, strikerBackset - filletR, 0]) {
       difference() {
         translate([0,0,0]) {
           cube([filletR+3, filletR+3, strikerHeight]);
         }
         translate([0,0,-small]) {
           cylinder(r=filletR, h=strikerHeight+small*2, $fn=48);
         }
       }
     }
   }
  }
}
   filletR = 20;


module latch()
{
  striker();
  mount();
  join();
}

difference() {
  latch();
  holes();
}
