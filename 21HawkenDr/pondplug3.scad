small = 0.1;
$fn = 32;

flangeH  = 2;
flangeR1 = 24/2;
flangeR2 = 24/2;

stemH = 12.0;
stemR1 = 24/2;
stemR2 = 20.0/2;
stemWall = 1;


module flange()
{
  cylinder(r1=flangeR1, r2=flangeR2, h=flangeH);
}

module stem()
{
  translate([0,0,flangeH]) {
    difference() {
      cylinder(r1=stemR1, r2=stemR2, h=stemH);
      translate([0,0,-small]) {
        cylinder(r=stemR2-stemWall, h=stemH+2*small);
      }
    }
  }
}

flange();
stem();
