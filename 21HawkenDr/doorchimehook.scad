small = 0.1;
$fn = 24;

baseX = 16.0;
baseY = 52.0;
baseZ = 3.0;
baseR = 2;
offsetError = 0.9;
pegR = 4.0/2;
pegH = 5.0+small;
pegPos = [baseX/2+offsetError,baseY/2,baseZ-small];

knobR1 = 8.0/2;
knobR2 = 8.5/2;
knobH = 4.0+small;
knobPos = [baseX/2,baseY/2,baseZ+pegH-small];

/*      
 *   Y  /\
 *     |\ \
 *     | \ \
 *   Z |  \ \
 *     |   \/
 *     +----
 *       X
 */
module brace(size)
{
  polyhedron (
    points = [
      [0, 0, size[2]],
      [0, size[1], size[2]], 
      [0, size[1], 0], 
      [0, 0, 0],
      [size[0], 0, 0],
      [size[0], size[1], 0]
    ],
    triangles = [[0,3,2],
                 [0,2,1],
                 [3,0,4],
                 [1,2,5], 
                 [0,5,4],
                 [0,1,5],
                 [5,2,4],
                 [4,2,3],
   ]
  );
}

module boxCornerNegative(centre, size, rotation)
{
  translate(centre) {
    rotate([0,0,rotation])
    {
      difference()
      {
        translate([-small,-small,-small/2]) {
          cube([size[0]+small,size[1]+small,size[2]+small]);
        }
        translate([size[0],size[1],-small]) {
          cylinder(h=size[2]+small*2,r=size[0], center=false);
        }
      }
    }
  }
}

module roundedBox(size, radius)
{
  difference()
  {
    cube(size);
    boxCornerNegative([0,0,0],[radius,radius,size[2]],0);
    boxCornerNegative([0,size[1],0],[radius,radius,size[2]],270);
    boxCornerNegative([size[0],0,0],[radius,radius,size[2]],90);
    boxCornerNegative([size[0],size[1],0],[radius,radius,size[2]],180);
  }
}

module base()
{
  roundedBox([baseX,baseY,baseZ], baseR);
}

module peg()
{
  translate(pegPos) {
    cylinder(r=pegR, h=pegH);
  }
}

module knob()
{
  translate(knobPos) {
    cylinder(r1=knobR1, r2=knobR2, h=knobH);
  }
}

supportX = 20;
supportZ = pegH+knobH;
supportY = pegR*2;
supportPos = [supportY/2+baseX/2,baseY/2,-small];
module support()
{
  intersection() {
    translate([knobPos[0],pegPos[1],pegPos[2]]) {
      cylinder(r=knobR1, h=pegH+small);
    }
    translate(pegPos) {
      translate([-pegR,0,-small]) {
        cube([pegR*2,knobR2+small,pegH+small*3]);
      }
    }
  }
}

base();
peg();
knob();
support();
