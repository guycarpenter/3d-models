
holeR = 6.0/2;
outerR = 16.0/2;
height = 6.5;
small = 0.1;
$fn = 64;

module hole()
{
  translate([0,0,-small]) {
    cylinder(r=holeR, h=height+small*2);  
  }
}

module washer()
{
  difference()
  {
    cylinder(r=outerR, h=height);
    hole();
  }
}

washer();
