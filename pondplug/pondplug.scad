hole_radius = 5.0;

top_radius = 40.0 / 2.0;
top_height = 0.0;

base_height = 5.0;
base_radius = 40.0 / 2.0;

recess_cross_radius = 3.4 / 2.0;
recess_radius = (32.5 - recess_cross_radius) / 2.0;

$fn = 100;

small = 1.0;

module top()
{
  translate([0,0,base_height])
  {
    cylinder(h=top_height, r=top_radius);
  }
}

module base()
{
  cylinder(h=base_height, r1=base_radius, r2=top_radius);
}

module hole()
{
  translate([0,0,-small])
  {
    cylinder(h=top_height+base_height+small*2, r=hole_radius);
  }
}

module recess()
{
  rotate_extrude(convexity = 10)
  {
    translate([recess_radius,0,0]) 
    {
      circle(r=recess_cross_radius);
    }
  }
}

difference()
{
  union()
  {
    //top();
    base();
  }
  union()
  {
    hole();
    recess();
  }
}

