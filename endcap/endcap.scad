outerDiam    = 19.5;
innerDiam    = 16.5;
sleeveLength = 34.0;
capLength    =  2.0;
small        =  1.0;

capLength    = -1.0;

difference() {
  cylinder(h=sleeveLength+capLength, r=outerDiam/2, $fa=1);
  translate([0,0,capLength])
    cylinder(h=sleeveLength+small, r=innerDiam/2, $fa=1);
}
