small       =  1;
mountDepth  =  5;
mountHeight = 10;

holeDiam    =  4;
counterDiam =  8;
counterDepth=  2;

topHeight   =  4;

steps     = 10;
ax0       =  0;
ax1       =  0;
ay0       =  0;
ay1       = 10;
az0       = 28;
az1       = 28;

bx0       = 10;
bx1       = 10;
by0       =  0;
by1       = 10;
bz0       = 28;
bz1       = 28;

cx0       = 10;
cx1       = 10;
cy0       =  0;
cy1       = 10;
cz0       = 26;
cz1       =  4;

module slice(step)
{
  f0       = step/steps;
  axn     = ax0 + (ax1-ax0)*f0;
  ayn     = ay0 + (ay1-ay0)*f0;
  azn     = az0 + (az1-az0)*f0;
  bxn     = bx0 + (bx1-bx0)*f0;
  byn     = by0 + (by1-by0)*f0;
  bzn     = bz0 + (bz1-bz0)*f0;
  cxn     = cx0 + (cx1-cx0)*f0;
  cyn     = cy0 + (cy1-cy0)*f0;
  czn     = cz0 + (cz1-cz0)*f0;

  f1      = (step+1)/steps;
  axm     = ax0 + (ax1-ax0)*f1;
  aym     = ay0 + (ay1-ay0)*f1;
  azm     = az0 + (az1-az0)*f1;
  bxm     = bx0 + (bx1-bx0)*f1;
  bym     = by0 + (by1-by0)*f1;
  bzm     = bz0 + (bz1-bz0)*f1;
  cxm     = cx0 + (cx1-cx0)*f1;
  cym     = cy0 + (cy1-cy0)*f1;
  czm     = cz0 + (cz1-cz0)*f1;

  polyhedron ( 
    points = [
      [axn, ayn, azn],
      [bxn, byn, bzn],
      [cxn, cyn, czn],
      [axm, aym, azm],
      [bxm, bym, bzm],
      [cxm, cym, czm]
    ],
    triangles = [
      [0,1,2], // front face
      [3,1,0], // top face
      [1,3,4],
      [1,5,2], // right face
      [5,1,4],
      [3,2,5], // left face
      [2,3,0],
      [5,4,3]  // back face
    ]
  );
}

module top()
{
  topX = bx1-ax1;
  topY = by1-by0;
  translate([0,0,bz1]) 
  {  
    cube(size=[topX,topY,topHeight]);
  }
}

module mount()
{
  mountX = bx1-ax1;
  translate([mountX/2, ,ay1-mountDepth/2, az1+mountHeight/2+topHeight])
  {
    difference() 
    {
      cube(size=[mountX, mountDepth, mountHeight], center=true);
      rotate(a=[90,0,0]) {
        cylinder(h=mountDepth+2*small, r=holeDiam/2, center=true);
      }
      rotate(a=[90,0,0]) {
        translate([0,0,mountDepth/2-counterDepth/2+small]) {
          cylinder(h=counterDepth+small, r=counterDiam/2, center=true);
        }
      }
    }
  }
}

top();
mount();
for (i = [0:steps-1])
  slice(i);
