// 19mm diam
//  4mm hole - eccentric
//  8mm high

small       =  1.0;
outerDiam   = 19.0;
holeDiam    =  4.0;
height      =  8.0;
sinkDiam    =  8.0;
sinkDepth   =  2.0;
eccen       =  2.0;  // hole offset from centre

module body()
{
  cylinder(h=height, r=outerDiam/2, $fa=1);
}

module sink()
{
  translate([eccen,0,height-sinkDepth]) {
    cylinder(h=sinkDepth+small, r=sinkDiam/2, $fa=1);
  }
}

module hole()
{
  translate([eccen,0,-small]) {
    cylinder(h=height+2*small, r=holeDiam/2, $fa=1);
  }
}

difference() {
  body();
  hole();
  sink();
}

  
