clipHeight =  5.0;
clipLength =  5.0;
clipSpace  = 75.0;

blockRadius = 200.0;
blockWidth  =  45.0;
blockHeight =  10.0;
blockLength =  clipSpace+clipLength*2;

$fa = 1;
$fs = 1;

module block_cyl()
{
  translate([0,blockWidth/2,-blockRadius+blockHeight])
    rotate(a=90, v=[1,0,0])
      cylinder(h=blockWidth, r=blockRadius);
}

module block_vol()
{
  translate([-blockLength/2, -blockWidth/2, 0])
    cube(size=[blockLength, blockWidth, blockHeight]);
}


module clip()
{
  translate([-clipSpace/2-clipLength,-blockWidth/2,-clipHeight])
    cube(size=[clipLength, blockWidth, clipHeight]);
}
module clips()
{
  clip();
  mirror([1,0,0]) clip();
}


module block()
{
  intersection()
  {
    block_cyl();
    block_vol();
  }

}


//block_cyl();
//block_vol();
block();
clips();
