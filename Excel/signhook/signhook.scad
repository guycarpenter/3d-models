small = 0.1;
large = 100;
$fn = 64;

/*      
 *   Y  /\
 *     |\ \
 *     | \ \
 *   Z |  \ \
 *     |   \/
 *     +----
 *       X
 */
module brace(size)
{
  polyhedron (
    points = [
      [0, 0, size[2]],
      [0, size[1], size[2]], 
      [0, size[1], 0], 
      [0, 0, 0],
      [size[0], 0, 0],
      [size[0], size[1], 0]
    ],
    triangles = [[0,3,2],
                 [0,2,1],
                 [3,0,4],
                 [1,2,5], 
                 [0,5,4],
                 [0,1,5],
                 [5,2,4],
                 [4,2,3],
   ]
  );
}

// a is angle between legs
h = 1037;
o = (572-12.23)/2;
a = asin(o/h);
angle = a * 2;

// outset above sleeve on each face
outset = (27.25-25)/2;

sleeveH = 25;
sleeveW = 14.0;
sleeveL = 3;
sleeveIn = 2;

arcH = sleeveH + outset;
arcW = sleeveW + 2 * outset;
arcIR = 12/2.0;  // print 1 was 10.5
arcOR = arcIR + arcW;

wedge1L = 14;
wedge1H = 14;
wedge1W = sleeveW;

drillY = (arcIR+arcOR)/2;
drillR = 1;

module arc()
{
    difference() {
        cylinder(r=arcOR, h=arcH);
        translate([0,0,-small]) {
          cylinder(r=arcIR, h=arcH+2*small);
          rotate(0,[0,0,1]) translate([-large/2,0,0]) cube([large,large,large]);
          rotate(a*2,[0,0,1]) translate([-large/2,0,0]) cube([large,large,large]);
          // hole
          rotate(a,[0,0,1])
            translate([0,-drillY,0]) cylinder(r=drillR,h=arcH+2*small);
        }
    }
}

module sleeve()
{
  translate([arcIR,0,0]) {
    cube([arcW,sleeveL,arcH]);
    translate([outset,sleeveL,0]) 
      cube([sleeveW,sleeveIn,sleeveH]);
    translate([wedge1W+outset,sleeveL,0]) 
      rotate(90, [0,0,1]) brace([wedge1L, wedge1W, wedge1H]);
  }
}

module cap()
{
  rotate(a*2, [0,0,1]) {
    translate([-arcW-arcIR,0,0]) {
      cube([arcW,sleeveL,arcH]);
    }  
  }
}

difference() {
  union() {
    arc();
    cap();  // non-inserting end cap
    sleeve();  // inserting end cap
  }
  // use this to print just a slice for size testing
  //translate([-large/2,-large/2,3]) cube([large,large,large]);
}
