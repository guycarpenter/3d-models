small = 0.01;

outerHeight    = 15.0;
outerOD        = 19.0;
outerThick     =  2.0;
outerZ         =  5.0;

innerHeight    = outerHeight;
innerOD        = 10.0;
innerThick     =  2.0;

shankHeight    =  7.0;
shankThick     =  3.0;
shankOD        = innerOD;

spokeThick     =  1.0;
spokeHeight    = outerHeight;
spokeZ         =  1.0;

$fa = 1;
$fs = 1;

module sleeve(height, radius, thickness)
{
  difference()
  {
    cylinder(h=height, r=radius);
    translate([0,0,-small])
      cylinder(h=height+small*2, r=radius-thickness);
  }
}


module shank()
{
  sleeve(shankHeight, shankOD/2., shankThick);
}

module inner()
{
  sleeve(innerHeight, innerOD/2., innerThick);
}

module outer()
{
  translate([0,0,outerZ])
    sleeve(outerHeight-outerZ, outerOD/2., outerThick);
}

module spoke()
{
  translate([0,0,(spokeHeight-spokeZ)/2.+spokeZ])
    cube(size=[outerOD-outerThick, spokeThick, spokeHeight-spokeZ], center=true);
}

module spokes()
{
  difference()
  {
    intersection()
    {
      union() {
        spoke();
        rotate(a=60, v=[0,0,1]) spoke();
        rotate(a=120, v=[0,0,1]) spoke();
      }
      cylinder(h=spokeHeight, r=outerOD/2-innerThick);
    }
    cylinder(h=spokeHeight, r=innerOD/2-innerThick);
  }
}

module cap()
{
    translate([0,0,outerHeight])
    {
      intersection()
      {
        difference()
        {
            sphere(r=outerOD/2);
            cylinder(h=outerOD+small, r=innerOD/2-innerThick, center=true);
        }
        translate([0,0,(outerOD+small)/2])
          cube(size=[outerOD+small, outerOD+small, outerOD+small], center=true);
      }
    }
}

shank();
inner();
outer();
spokes();
cap();
