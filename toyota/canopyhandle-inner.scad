endC2C = 38.1; // 1.5 inches
endIR = 5.0/2.0;  // expanded to countersink limit, so no countersink any more
endOR = 16.0/2.0;

baseHeight = 8.0;
centerOR = 23.0/2.0;

shaftIR = 11.0/2.0;
shaftOR = 15.5/2.0;
endShaftOR = 8.0/2;  // sleeve radius
shaftHeight = baseHeight + 4;

small = 0.01;

$fs = 1;
$fa = 1;

module base()
{
  hull()
  {
    translate([-endC2C/2,0,0]) {
      cylinder(r=endOR, h=baseHeight);
    }
    cylinder(r=centerOR, h=baseHeight);
    translate([endC2C/2,0,0]) {
      cylinder(r=endOR, h=baseHeight);
    }
  }        
}

module shaft()
{
  cylinder(r=shaftOR, h=shaftHeight);
  for (c = [-endC2C/2, endC2C/2]) {
    translate([c,0,0]) {
      cylinder(r=endShaftOR, h=shaftHeight);
    }
  }
}

module shaft_negative()
{
  translate([0,0,-small]) {
    cylinder(r=shaftIR, h=shaftHeight+2*small);
  }
}

module end_negative()
{
  for (c = [-endC2C/2, endC2C/2]) {
    translate([c,0,-small]) {
      cylinder(r=endIR, h=shaftHeight+2*small);
   }
  }
}

difference() 
{
  union() {
    base();
    shaft();
  }
  shaft_negative();
  end_negative();
}

