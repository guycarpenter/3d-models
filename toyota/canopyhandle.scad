
endC2C = 38.1; // 1.5 inches
endIR = 5.0/2.0;
endOR = 16.0/2.0;
counterSinkIR = 8.5/2.0;
counterSinkDepth = 1.0;

baseHeight = 8.0;
centerOR = 23.0/2.0;

shaftIR = 11.0/2.0;
shaftOR = 15.5/2.0;
shaftHeight = baseHeight + 4;

small = 0.01;

$fs = 1;
$fa = 1;

module base()
{
  hull()
  {
    translate([-endC2C/2,0,0]) {
      cylinder(r=endOR, h=baseHeight);
    }
    cylinder(r=centerOR, h=baseHeight);
    translate([endC2C/2,0,0]) {
      cylinder(r=endOR, h=baseHeight);
    }
  }        
}

module shaft()
{
  cylinder(r=shaftOR, h=shaftHeight);
}

module shaft_negative()
{
  translate([0,0,-small]) {
    cylinder(r=shaftIR, h=shaftHeight+2*small);
  }
}

module end_negative()
{
  for (c = [-endC2C/2, endC2C/2]) {
    translate([c,0,-small]) {
      cylinder(r=endIR, h=baseHeight+2*small);
      cylinder(r=counterSinkIR, h=counterSinkDepth+small);
      translate([0,0,counterSinkDepth])
      {
        cylinder(r1=counterSinkIR, r2=endIR, h=counterSinkIR-endIR); // 45 deg
      }
   }
  }
}

difference() 
{
  union() {
    base();
    shaft();
  }
  shaft_negative();
  end_negative();
}


