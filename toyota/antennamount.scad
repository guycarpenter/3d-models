bottomIR = 7.0/2.0;
bottomOR = 14.0/2.0;

topIR = 12.0/2.0;
topOR = topIR+3.5;

bottomLength = 70.0;
topLength = 30.0;
overlapLength = 20.0;

module top()
{
  translate([0,0,bottomLength-overlapLength])
  {
    difference() 
    {
      cylinder(h=topLength+overlapLength, r=topOR);
      cylinder(h=topLength+overlapLength, r=topIR);
    }
  }
}

module bottom()
{
  difference()
  {  
    cylinder(h=bottomLength, r=bottomOR);
    cylinder(h=bottomLength, r=bottomIR);
  }
}

top();
bottom();
