pegLength = 10.0;
pegOR = 7.5/2.0;  // for 8.5/2 hole

$fs = 1;
$fa = 1;

module peg()
{
  intersection() 
  {
    rotate([0,90,0]) {
      cylinder(h=pegLength, r=pegOR);
    }
    translate([0,-pegOR,0]) 
    {
      cube([pegLength, pegOR*2, pegOR]);
    }
  }
}

translate([-pegLength/2, pegOR*1.5, 0])  // center on plate
{
  peg();
  translate([0,-pegOR*3,0]) 
  {
    peg();
  }
}
