small = 0.01;
big = 1000.0;

endC2C = 38.1; // 1.5 inches
endIR = 5.0/2.0;
endOR = 16.0/2.0;
countersinkIR = 9.0/2.0;
countersinkHeight = countersinkIR-endIR;

totalHeight = 16.5;

baseHeight = 8;
sliceHeight = 3.5;
centerOR = 33.0/2.0;

castleHeight = totalHeight-baseHeight;
castleOR = 28.0/2;

guideHeight = 2.5;
guideOR = 20.0/2.0;
guideWidth = 4.0; // width of peg being accommodated

shaftIR = 17.0/2.0;
endShaftOR = 8.0/2;  // alignment peg
stemHeight = 4;

sleeveIR = 23.0/2;
sleeveOR = 26.0/2;
sleeveHeight = 5.0;

recessDrop = sliceHeight;
recessR1 = 25.0/2; // NEED SUPPORTS FOR THIS
recessR2 = shaftIR;
recessHeight = recessR1-recessR2;

$fs = 1;
$fa = 1;

module base()
{
  hull()
  {
    translate([-endC2C/2,0,0]) {
      cylinder(r=endOR, h=baseHeight);
    }
    cylinder(r=centerOR, h=baseHeight);
    translate([endC2C/2,0,0]) {
      cylinder(r=endOR, h=baseHeight);
    }
  }  

  // end alignment guides
  for (c = [-endC2C/2, endC2C/2]) {
    translate([c,0,-stemHeight]) {
      cylinder(r=endShaftOR, h=stemHeight);
    }
  }
}

module castle()
{
  translate([0,0,baseHeight]) 
  {
    cylinder(r=castleOR, h=castleHeight);
  }
  translate([0,0,baseHeight+castleHeight]) 
  {
    difference() 
    { 
      cylinder(r=sleeveOR, h=sleeveHeight);
      cylinder(r=sleeveIR, h=sleeveHeight+small);
    }
  }
}

module recess_negative()
{
  translate([0,0,-small]) {
    cylinder(r=recessR1, h=recessDrop+small*2);
  }
}

module shaft_negative()
{
  translate([0,0,-small]) {
    cylinder(r=shaftIR, h=baseHeight+castleHeight+2*small);
  }
}

module end_negative()
{
  for (c = [-endC2C/2, endC2C/2]) {
    translate([c,0,0]) {
      translate([0,0,-stemHeight-small]) {
        cylinder(r=endIR, h=baseHeight+stemHeight+2*small);
      }
      translate([0,0,baseHeight-countersinkHeight-small]) 
      {
        cylinder(r1=endIR, r2=countersinkIR, h=countersinkHeight+small*2);
      }
   }
  }
}

module guide_negative()
{
  translate([0,0,baseHeight+castleHeight-guideHeight])
  {
    intersection() 
    {
      cylinder(r=guideOR, h=guideHeight+small);
      translate([-guideWidth/2,-guideWidth/2,0]) {
        cube(guideOR+guideWidth/2+small, guideOR+guideWidth/2+small, guideHeight);
      }
    }
  }       
}

module outer() {
  difference() 
  {
    union() {
      base();
      castle();
    }
    recess_negative();
    shaft_negative();
    end_negative();
    guide_negative();
  }
}

showTop = true;
//showTop = false;

dz = showTop ? sliceHeight : sliceHeight-big;

intersection() {
  outer();
  translate([-big/2,-big/2,dz]) {
    cube(big,big,big);
  }
}

