
largeIR = 126.2/2;
smallIR = 100.2/2;
wallThickness = 3.0;
floorThickness = 3.0;
totalHeight = 35.6;
highQ = 360;
lowQ = 16;
small = 0.1;
large = 1000;

floorSegments = 16;

module body()
{
  difference() {
    cylinder(r1=smallIR+2*wallThickness, r2=largeIR+2*wallThickness, h=totalHeight, $fn=highQ);
    translate([0,0,0]) {
      difference() {
        cylinder(r1=smallIR, r2=largeIR, h=totalHeight+small, $fn=highQ);
        cylinder(r=largeIR, h=floorThickness, $fn=highQ);
      }
    }
  }
}

module floorSegment()
{
  slotStep = 3.0;
  slotMin = 15;
  for (i=[0:8]) {
    assign(r = slotMin + slotStep * i) 
    {
      floorSlot(r, floorSegments);
    }
  }
  for (i=[9:12]) {
    assign(r = slotMin + slotStep * i)
    {
      floorSlot(r, floorSegments*2);
      rotate(360/floorSegments/2, [0,0,1]) floorSlot(r, floorSegments*2);
    }
  }
}

module floorSlot(r, segments)
{
  slotWidth = 1.5;
  gapWidth = 1.5;
  gapA = asin((gapWidth+slotWidth)/r);
  a = 360/segments - gapA;

  translate([0,0,-small]) {
    difference() {
      cylinder(r=r, h=floorThickness+small*2, $fn=highQ);  // outer arc
      translate([0,0,-small])
        cylinder(r=r-slotWidth, h=floorThickness+small*4, $fn=highQ);  // inner arc
      translate([-large/2,-large,-small]) 
        cube([large,large,floorThickness+small*4]);  // clip at 0 degrees
      rotate(180+a, [0,0,1])
        translate([-large/2,-large,-small]) 
          cube([large,large,floorThickness+small*4]);  // clip at a degrees
    }
    translate([r-slotWidth/2,0,0])
      cylinder(r=slotWidth/2, h=floorThickness+small*2, $fn=lowQ);  // round end 1 of slot
    rotate(a, [0,0,1])
      translate([r-slotWidth/2,0,0])
        cylinder(r=slotWidth/2, h=floorThickness+small*2, $fn=lowQ);  // round end 2 of slot
  }
}

module floorNegative()
{
  for (i=[0:floorSegments]) {
  //for (i=[0:0]) {
    rotate(i * 360 / floorSegments, [0,0,1]) floorSegment();
  }
}

module wallSlot(a)
{
  slotWidth = 1.5;
  slotLength = 10.0;
  slotAngle = 45.0;
  rotate(-slotAngle,[0,1,0]) {
    rotate(-90,[1,0,0]) {
      translate([-slotLength/2,0,-1]) {
        translate([slotWidth/2,-slotWidth/2,0]) cube([slotLength-slotWidth,slotWidth,large]);
        translate([slotWidth/2,0,0]) cylinder(r=slotWidth/2, h=large, $fn=lowQ);
        translate([slotLength-slotWidth/2,0,0]) cylinder(r=slotWidth/2, h=large, $fn=lowQ);
      }
    }
  }
}

module wallNegative()
{
  slotH0 = 8;
  slotDH = 8;
  slotDA = 3.5;
  for (h=[0:1]) {
    for (i=[0:12]) {
      translate([0,0,slotH0+slotDH*h]) {
        rotate(slotDA*i,[0,0,1]) {
          wallSlot();
        }
      }
    }
  }
}

difference()
{
  body();
  floorNegative();
  wallNegative();
}

