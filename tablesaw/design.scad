small = 0.1;

wormOR = 32.0/2;
wormIR = 12.5/2;  // printed at 12, too tight
wormH  = 21.3;

capOR = 22.0/2;
capH = 8.5;   // printed at 8.8, too long

notchW = 3.48;
notchD = 6.5;
notchPos = [0,0,wormH+capH-notchD+notchW/2];

module worm()
{
  cylinder(r=wormOR, h=wormH);
}

module cap()
{
  translate([0,0,wormH])
    cylinder(r=capOR, h=capH);
}

module shaft()
{
  translate([0,0,-small])
    cylinder(r=wormIR, h=wormH+capH+2*small);
}

module notch()
{
  translate(notchPos) {
    rotate([0,90,0]) translate([0,0,-small]) cylinder(h=capOR*2+2*small, r=notchW/2, center=true, $fn=180);
    translate([-small-capOR,-notchW/2,0]) cube([capOR*2+2*small, notchW, notchD-notchW/2], center=false);
  }
}

nutR = 18.29 / 2;
module nutRecess()
{
  cylinder(r=nutR, $fn=6, h=6.07);
}
