cube();
small = 10;

nPanels = 2;
panelY = 5200 / (nPanels*2+1);
leftSize = [5200,5200,210];
rightSize = [5200,5200,210];
panelSize = [1000,panelY,210+small*2];
panelStep = panelSize[1]*2;
panelOffset = (leftSize[1]-panelSize[1]*(nPanels*2-1))/2;
module left()
{
  translate([0,0,-panelSize[2]/2]) {
    difference() {
      cube(leftSize);
      for (y = [0 : nPanels-1]) {
        translate([-small,panelOffset + y*panelStep-small,-small]) {
          cube(panelSize);
        }
      }
    }
  }
}


module right()
{
  translate([-rightSize[0]-10,0,-panelSize[2]/2]) {
    {
      cube(leftSize);
      for (y = [0 : nPanels-1]) {
        translate([rightSize[0]-small,panelOffset + y*panelStep-small,-small]) {
          cube(panelSize);
        }
      }
    }
  }
}

angle = 20;

translate([0,0,sin(angle)*leftSize[0]]) {
  rotate([0,angle,0]) {
    left();
  }
}

translate([0,0,sin(angle)*leftSize[0]]) {
  rotate([0,-angle,0]) {
    right();
  }
}
