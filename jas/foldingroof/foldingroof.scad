small = 10;

basePitch = 5;
baseDx = 6000;
baseDy = 4000;
baseMinZ = 100;
baseMaxZ = baseMinZ + sin(basePitch) * baseDy;

roofPitch = 2;
wallThickness = 200;
wallOuterX = 3000 + wallThickness * 2;
wallOuterY = 2500 + wallThickness * 2;
wallFreeZ  = 800;
wallMinZ   = wallFreeZ + baseMaxZ-baseMinZ;
wallMaxZ   = wallMinZ + wallOuterX/2 * sin(roofPitch);

module roofNegative()
{
  rotate([0,roofPitch,0]) {
    translate([0,0,wallMaxZ]) {
      cube([wallOuterX*2,wallOuterY*2,wallMaxZ*2], center=true);
    }
  }
  rotate([0,-roofPitch,0]) {
    translate([0,0,wallMaxZ]) {
      cube([wallOuterX*2,wallOuterY*2,wallMaxZ*2], center=true);
    }
  }
}

module roomNegative()
{
  cube([wallOuterX-wallThickness*2, wallOuterY-wallThickness*2, wallMaxZ+small*2], center=true);
}

windowFoot = 100;
windowHead = 100;
module windowNegative()
{
  volumeA =[wallOuterX-wallThickness*2, wallOuterY*2, wallFreeZ-windowFoot-windowHead];
  translate([-volumeA[0]/2,-volumeA[1]/2,-wallMaxZ+wallMinZ-wallFreeZ+windowFoot]) {
    cube(volumeA);
  }

  volumeB =[wallOuterX*2, wallOuterY-wallThickness*2, wallFreeZ-windowFoot-windowHead];
  translate([-volumeB[0]/2,-volumeB[1]/2,-wallMaxZ+wallMinZ-wallFreeZ+windowFoot]) {
    cube(volumeB);
  }
}

module room()
{
  difference() {
    translate([0,0,-wallMaxZ/2]) {
      difference() {
        cube([wallOuterX,wallOuterY, wallMaxZ], center=true);
        roomNegative();
      }
    }
    roofNegative();
    windowNegative();
  }
  base();
  roofJoints();
  mirror([1,0,0]) roofJoints();
}

panelOverhang = 600;
panelX = wallOuterX/2 / cos(roofPitch) + panelOverhang;  // one side only
panelY = wallOuterY + 2 * panelOverhang;
panelZ = 100;

jointR = 100;
jointH = 100;
jointZ = 100; // top to centre
jointIR = 20;
jointSpacing = panelX-1400;  // in 700 from each end

module joint()
{
  translate([0,jointH,-jointZ]) {
    rotate([90,0,0]) {
      difference() {
        union() {
          cylinder(r=jointR, h=jointH);
          translate([-jointR,0,0]) {
            cube([jointR*2, jointZ, jointH]);
          }
        }
        translate([0,0,-small]) cylinder(r=jointIR, h=jointH+small*2);
      }
    }
  }
}

roofJointZ = baseMaxZ;
module roofJoint()
{
  translate([0,jointH,jointZ]) {
    rotate([90,0,0]) {
      difference() {
        union() {
          cylinder(r=jointR, h=jointH);
          translate([-jointR,-roofJointZ,0]) {
            cube([jointR*2, roofJointZ, jointH]);
          }
          translate([-jointR-small,-roofJointZ,-jointH]) {
            cube([jointR*2+small*2, roofJointZ-jointH, jointH*3]);
          }
        }
        translate([0,0,-small]) cylinder(r=jointIR, h=jointH+small*2);
      }
    }
  }
}

joint1Pos = [700, -panelY/2+panelOverhang/2, 0];
joint2Pos = [700+jointSpacing, -panelY/2+panelOverhang/2, 0];
joint3Pos = [700, panelY/2-panelOverhang/2-jointH, 0];
joint4Pos = [700+jointSpacing, panelY/2-panelOverhang/2-jointH, 0];

module panelJoints()
{
  rotate([0,roofPitch,0]) {
    translate(joint1Pos) joint();
    translate(joint2Pos) joint();
    // hack to fix error in printing base - downslope joints are in wrong place
    translate([0,jointH,0]) {
      translate(joint3Pos) joint();
      translate(joint4Pos) joint();
    }
  }
}

roofJointOutset = 1600;
roofJoint1Pos = [-roofJointOutset, -panelY/2+panelOverhang/2, -850];
roofJoint2Pos = [-roofJointOutset-(panelX-1400), -panelY/2+panelOverhang/2, -850];
roofJoint3Pos = [-roofJointOutset, panelY/2-panelOverhang/2, -850];
roofJoint4Pos = [-roofJointOutset-(panelX-1400), panelY/2-panelOverhang/2, -850];

module roofJoints()
{
  translate(roofJoint1Pos) roofJoint();
  translate(roofJoint2Pos) roofJoint();
  translate(roofJoint3Pos) roofJoint();
  translate(roofJoint4Pos) roofJoint();
}

large = 10000;

module panel()
{
  difference() {
    rotate([0,roofPitch,0]) {
      translate([-panelZ,-panelY/2,0]) {
        cube([panelX+panelZ,panelY,panelZ]);
      }
    }
    translate([-large,-large/2,-large/2]) cube([large,large,large]);
  }
  panelJoints();
}

module roof()
{
  //panel();
  mirror([1,0,0]) panel();
}

module strut(length)
{
  difference() {
    union() {
      cylinder(r=jointR, h=jointH);
      translate([length,0,0]) cylinder(r=jointR, h=jointH);
      translate([0,-jointR,0]) cube([length,jointR*2,jointH]);
    }
    translate([0,0,-small]) cylinder(r=jointIR, h=jointH*2*small);
    translate([length,0,-small]) cylinder(r=jointIR, h=jointH*2*small);
  }
}

module strutSet() {
  translate([150,150,0]) strut(1100);
  translate([150,450,0]) strut(1100);
  translate([150,750,0]) strut(1100);
}

scale = 1/50;

module printRoof()
{
  scale(scale) {
    rotate([0,0,90]) {
      rotate([0,180-roofPitch,0]) panel();
    }
  }
}

module printRoom1()
{
  scale(scale) {
    rotate([0,0,90]) {
      difference() {
        room();
        translate([-large/2,-large/2,0-(wallMaxZ-wallMinZ)-windowHead-small]) {
          cube([large,large,large]);
        }
      }
    }
  }        
}

module printRoom2()
{
  scale(scale) {
    rotate([0,0,90]) {
      intersection() {
        room();
        translate([-large/2,-large/2,0-(wallMaxZ-wallMinZ)-windowHead]) {
          cube([large,large,large]);
        }
      }
    }
  }        
}

module printStruts()
{
  scale(scale) {
    strutSet();
    mirror([0,1,0]) strutSet();
  }
}

module base()
{
  translate([0,0,-wallFreeZ-baseMaxZ/2]) {
    difference() {
      cube([baseDx, baseDy, baseMaxZ], center=true);
      roomNegative();
      translate([-large/2,-large/2,baseMaxZ]) {
        rotate([-basePitch,0,0]) {
          cube([large, large, large]);
        }
      }
    }
  }
}

/*
room();
roof();
base();
*/

printRoof();
//printRoom1();
//printRoom2();
//printStruts();

//roofJoints();
