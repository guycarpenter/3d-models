#!/usr/bin/env python

from math import cos, sin, pi
from openscad import *

wormOR = 32.0/2

threadStep = 1
threadPitch = 19.53 / 4
threadOH = 3.67
threadIH = 1.50
threadD  = 3.00

threadOR = wormOR
threadIR = wormOR - threadD

threadZ0 = 0
threadZ1 = (threadOH-threadIH)/2
threadZ2 = (threadOH-threadIH)/2 + threadIH
threadZ3 = threadOH

threadX0 = wormOR
threadX1 = wormOR-threadD


def crossSection(a):
  dz = threadPitch / 360 * a
  arad = (90-a) / 180.0 * pi
  
  x0 = cos(arad) * threadOR;
  y0 = sin(arad) * threadOR;
  z0 = threadZ0 + dz;

  x1 = cos(arad) * threadIR;
  y1 = sin(arad) * threadIR;
  z1 = threadZ1 + dz;

  x2 = cos(arad) * threadIR;
  y2 = sin(arad) * threadIR;
  z2 = threadZ2 + dz;

  x3 = cos(arad) * threadOR;
  y3 = sin(arad) * threadOR;
  z3 = threadZ3 + dz;

  return [[x0,y0,z0],[x1,y1,z1],[x2,y2,z2],[x3,y3,z3]]


    
def thread():
  polyhedron = OpenscadPolyhedron()
  polyhedron.convexity = 10
  p = 0
  first = True
  for a in range(-360,360*5,threadStep):
      print a
      for points in crossSection(a):
        polyhedron.points.append(points)
        p += 1
      if first:
          first = False
          polyhedron.triangles.append([0,1,2]) # first end
          polyhedron.triangles.append([2,3,0])
      else:
          n = p - 8
          polyhedron.triangles.append([n+0,n+4,n+5])
          polyhedron.triangles.append([n+5,n+1,n+0])
          polyhedron.triangles.append([n+0,n+3,n+7])
          polyhedron.triangles.append([n+7,n+4,n+0])
          polyhedron.triangles.append([n+1,n+5,n+6])
          polyhedron.triangles.append([n+6,n+2,n+1])
          polyhedron.triangles.append([n+2,n+6,n+7])
          polyhedron.triangles.append([n+7,n+3,n+2])


  n = p - 4
  polyhedron.triangles.append([n+3,n+2,n+1])
  polyhedron.triangles.append([n+1,n+0,n+3])  # final end cap

  return polyhedron

threadFile = open("thread.scad", "w")          
poly = thread()
print poly
threadFile.write("module thread() {\n");
poly.write(threadFile)
threadFile.write("\n}\n");
threadFile.close();

