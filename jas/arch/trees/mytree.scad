stemScale = 0.9;
stemR1 = 10;
stemR2 = stemR1 * stemScale;
stemH = 10;

branchScale = 0.9;
branchR1 = stemR2;
branchR2 = branchR1 * branchScale;
branchH = 10;
branchA = 12;

ARotate = [branchA, 0, 60];
BRotate = [-branchA, 0, 60];

$fn = 24;

module node(depth=9)
{
  cylinder(r1=stemR1, r2=stemR2, h=stemH);

  translate([0,0,stemH]) {
    rotate(a=ARotate) {
      cylinder(r1=branchR1, r2=branchR2, h=branchH);
      if (depth > 0) {
        translate([0,0,branchH]) {
          scale(branchScale*stemScale) {
            node(depth-1);
          }
        }
      }
    } 
  }

  translate([0,0,stemH]) {
    rotate(a=BRotate) {
      cylinder(r1=branchR1, r2=branchR2, h=branchH);
      if (depth>0) {
        translate([0,0,branchH]) {
          scale(branchScale*stemScale) {
            node(depth-1);
          }
        }
      }
    } 
  }
}

node();

