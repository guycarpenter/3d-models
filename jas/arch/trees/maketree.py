#!/usr/bin/env python

# TODO - need a general transformation matrix such that we can
# - initialize as unit
# - apply a scale function
# - apply a translation
# - apply a rotation
# - translate a point

from math import cos, sin, pi
from openscad import *
import numpy
import transformations
import random

stemR1 = 1.0
stemR2 = 0.9
stemLength  = 4.0
forkAngle0  = 15.0
forkRange = 10
forkLength = 2.0
lengthScale = 0.9
radiusScale = 0.8
fn = 16
maxDepth = 10

def rotate_list(l,n):
    return l[n:] + l[:n]

def identity():
    return transformations.identity_matrix()

def scale(M, s):
    return transformations.concatenate_matrices(M, transformations.scale_matrix(s))

def translate(M, translation):
    return transformations.concatenate_matrices(M, transformations.translation_matrix(translation))

def rotate(M, angle, direction, point=None):
    return transformations.concatenate_matrices(M, transformations.rotation_matrix(angle, direction, point))

# there must be a cleaner way of doing this
def project(M, v):
    v1 = numpy.dot(M, [v[0],v[1],v[2],1])
    return [v1[0],v1[1],v1[2]]

def bottomCap(poly):
    circle = []
    c = [0,0,0]
    pc = len(poly.points)
    poly.points.append(c)

    point0 = len(poly.points)
    for i in range(0,fn):
        j = (i + 1) % fn
        aRad = 360.0 / fn * i /180.0 * pi
        x = stemR1 * sin(aRad)
        y = stemR1 * cos(aRad)
        circle.append(len(poly.points))
        poly.points.append([x,y,0])
        poly.triangles.append([pc,point0+j,point0+i])

    return circle

# circle is a set of fn point indeces for the base of the stem

def node(M, poly, depth, circle, radius, length):

    forkAngle = forkAngle0 + random.randint(-forkRange,+forkRange)
    forkAngleRad = forkAngle / 180.0 * pi
    invForkAngleRad = (90 - forkAngle) / 180.0 * pi
    forkZScale = sin(pi/2 - invForkAngleRad)

    point0 = len(poly.points)

    for i in range(0,fn):
        j = (i + 1) % fn
        aRad = 360.0 / fn * i /180.0 * pi
        x = radius * sin(aRad)
        y = radius * cos(aRad)
        
        if depth == maxDepth:
            # flat top
            z = length
        else:
            # bifurcation
            z = length + sin(invForkAngleRad) * abs(x) - forkZScale * radius * abs(sin(aRad))

        poly.points.append(project(M,[x,y,z]))

        poly.triangles.append([circle[i],circle[j],point0+i])
        poly.triangles.append([point0+i,circle[j],point0+j])

    if depth == maxDepth:
        c = [0,0,z]
        pc = len(poly.points)
        poly.points.append(project(M,c))
            
        for i in range(0,fn):
            j = (i + 1) % fn
            poly.triangles.append([point0+i,point0+j,pc])

    else:
        circle1 = []
        circle2 = []
        shared = []
        for i in range(0,fn/2+1):
            circle1.append(point0+(fn+i)%fn)
            circle2.append(point0+(fn-i)%fn)
            
        for j in range(i+1,fn):
             aRad = 360.0 / fn * j / 180.0 * pi
             x = 0
             y = radius * cos(aRad)
             z = length - radius * sin(aRad) / cos(forkAngleRad)
             pn = len(poly.points)
             poly.points.append(project(M,[x,y,z]))
             shared.append(pn)
        circle1 = circle1 + shared
        circle2 = circle2 + shared
        circle1 = rotate_list(circle1, -fn/4)
        circle2 = rotate_list(circle2, fn/4+1)
        circle2.reverse()

        M1 = M
        M1 = translate(M1, [0,0,length])
        M1 = rotate(M1,forkAngle / 180.0 * pi, [0,1,0]) 
        M1 = rotate(M1,90.0 / 180.0 * pi, [0,0,1]) 
        node(M1, poly, depth+1, circle1, radius * radiusScale, length * lengthScale)

        M2 = M
        M2 = translate(M2, [0,0,length])
        M2 = rotate(M2,-forkAngle / 180.0 * pi, [0,1,0]) 
        M2 = rotate(M2,90.0 / 180.0 * pi, [0,0,1]) 
        node(M2, poly, depth+1, circle2, radius * radiusScale, length * lengthScale)
        
        
        

poly = OpenscadPolyhedron()
poly.convexity = 10

node(identity(), poly, 1, bottomCap(poly), stemR1, forkLength)

threadFile = open("tree.scad", "w")
threadFile.write("module tree() {\n");
poly.write(threadFile)
threadFile.write("\n}\n");
threadFile.close();

