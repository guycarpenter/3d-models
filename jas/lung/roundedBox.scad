small = 0.1;

module roundedBox(size, radii)
{
  difference()
  {
    cube(size);
    boxCornerNegative([0,0,0],[radii[0],radii[0],size[2]],0);
    boxCornerNegative([0,size[1],0],[radii[1],radii[1],size[2]],270);
    boxCornerNegative([size[0],0,0],[radii[2],radii[2],size[2]],90);
    boxCornerNegative([size[0],size[1],0],[radii[3],radii[3],size[2]],180);
  }
}

// cylinders on the Y axis instead of default Z-axis
module roundedBoxY(size, radii)
{
  translate([0,size[1],0]) {
    rotate(90,[1,0,0]) {
      roundedBox([size[0],size[2],size[1]], radii);
    }
  }
}

module boxCornerNegative(centre, size, rotation)
{
  translate(centre) {
    rotate([0,0,rotation])
    {
      difference()
      {
        translate([-small,-small,-small/2]) {
          cube([size[0]+small,size[1]+small,size[2]+small]);
        }
        translate([size[0],size[1],-small]) {
          cylinder(h=size[2]+small*2,r=size[0], center=false);
        }
      }
    }
  }
}
