use <elliptical.scad>;
use <nose.scad>;
use <shield.scad>;
use <roundedBox.scad>;

small = 0.1;
$fn = 64;

bottleD = 53.5;
bottleR = bottleD/2;
bottleH = 125.0;  // to bottom of extruding pipes

backOffset = 5.0; // between inner ring and back support

baseWall = 5.0;
baseIR = bottleR + 0.5;  // lee
baseOR = bottleR + baseWall;
baseFloor = 2.0;
baseHoleR = 20;
baseH = 20.0;

beltWidth = 80;
beltHeight = baseH;
beltWall = 5.0;
beltPos = [-beltWidth/2,-baseIR-backOffset-beltWall,0];

beltHoleInset = 3.0;
beltHoleHeight = beltHeight - 8;
beltHoleWidth = 3.0;

ringWall = 3.0;
ringIR = baseIR;
ringOR = ringIR + ringWall;
ringZ = 90;
ringHeight = 20;
ringArc = 45;  // angle cut away
ringCornerR = 4;
ringBaseWidth = 20;
ringBaseWall = 5.0;


shoulderHeight = baseH;
shoulderWall = beltWall;
shoulderWidth = 40; // per side
shoulderAngle = 50;
shoulderPos = [0,-ringIR-backOffset-shoulderWall,ringZ+ringHeight/2];
shoulderHoleInset1 = 5;
shoulderHoleInset2 = 11;
shoulderHoleWidth = 3;
shoulderHoleHeight = shoulderHeight - 8;

resevoirOR = bottleR + 3;
resevoirWall = 3.0;
resevoirHeight = 50;
resevoirTopZ = 30;
resevoirCapOR = resevoirOR;
resevoirCapIR = resevoirCapOR - 10;
resevoirCapHeight = 3;

gaugeR = 15;
gaugeThickness = 4;
gaugeAxleR = 2.5/2;
gaugeAxlePos = [0,-6,-small];
gaugePinR = 4.8/2;
gaugePinH = 10;
gaugePinPos1 = [-27/2,0,-gaugePinH];
gaugePinPos2 = [ 27/2,0,-gaugePinH];
gaugeOutset = 21;

module bottle()
{
  translate([0,0,baseFloor]) {
    cylinder(r=bottleR, h=bottleH);
  }
}

module base()
{
  // base
  translate([-ringBaseWidth/2,-ringIR-backOffset,0]) {
    roundedBoxY([ringBaseWidth, ringBaseWall, ringHeight],[3,3,3,3]);
  }

  difference() {
    cylinder(r=baseOR, h=baseH);
    translate([0,0,baseFloor]) cylinder(r=baseIR, h=baseH-baseFloor+small);
    translate([0,0,-small]) cylinder(r=baseHoleR,h=baseFloor+small*2);
  }
}

module cylinderSegment(r,h,a)
{
  difference()
  {
    cylinder(r=r, h=h);
    polyhedron(
      points = [
        [0,0,-small],
        [r+small*sin(a), r+small, -small],
        [-r+small*sin(a), r+small, -small],
        [0,0,h+small],
        [r+small*sin(a), r+small, h+small],
        [-r+small*sin(a), r+small, h+small]
      ],
      triangles = [
        [0,1,2],  // bottom
        [5,4,3],  // top
        [1,0,3],  // f1
        [4,1,3],
        [2,1,4],  // f2
        [5,2,4],
        [3,2,5],  // f3
        [0,2,3]
      ]
    );
  }
}

module ringCorner(r,a)
{
  rotate(-a,[0,0,1]) {
    rotate(90,[1,0,0]) {
      translate([0,0,-ringIR-ringWall*1.5]) {
        boxCornerNegative([0,0,0],[r,r,ringWall*2],0);
      }
    }
  }
}
module ringCornerPair(r)
{
  ringCorner(r,ringArc);
  translate([0,0,ringHeight]) mirror([0,0,1]) ringCorner(r,ringArc);
}

module ringCorners(r)
{
  ringCornerPair(r);
  mirror([1,0,0]) ringCornerPair(r);
}

module ring()
{
  // base
  translate([-ringBaseWidth/2,-ringIR-backOffset,ringZ]) {
    roundedBoxY([ringBaseWidth, ringBaseWall, ringHeight],[3,3,3,3]);
  }

  translate([0,0,ringZ]) {
    difference() {
      cylinderSegment(r=ringOR, h=ringHeight, a=ringArc);
      translate([0,0,-small]) {
        cylinder(r=ringIR, h=ringHeight+small*2);
      }
      ringCorners(ringCornerR);
    }
  }
}

module belt()
{
  translate(beltPos) {
    difference() {
      roundedBoxY([beltWidth, beltWall, beltHeight],[3,3,3,3]);
      translate([beltHoleInset,0,(beltHeight-beltHoleHeight)/2]) beltHole();
      translate([beltWidth-beltHoleInset-beltHoleWidth,0,(beltHeight-beltHoleHeight)/2]) beltHole();
    }
  }
}



module beltHole()
{
  translate([0,-small,0]) {
    cube([beltHoleWidth,beltWall+small*2, beltHoleHeight]);
  }
}

module shoulder()
{
  leftShoulder();
  rightShoulder();
  translate([-shoulderHeight/2,shoulderPos[1],0]) {
    cube([shoulderHeight,shoulderWall,ringZ+ringHeight]);
  }
}
module rightShoulder()
{
  mirror([1,0,0]) leftShoulder();
}

module shoulderHole()
{
  cube([shoulderHoleWidth, shoulderWall+2*small,shoulderHoleHeight]);
}

module shoulderHoles()
{
  translate([shoulderWidth-shoulderHoleWidth-shoulderHoleInset1,-small,(shoulderHeight-shoulderHoleHeight)/2]) {
    shoulderHole();
  }
  translate([shoulderWidth-shoulderHoleWidth-shoulderHoleInset2,-small,(shoulderHeight-shoulderHoleHeight)/2]) {
    shoulderHole();
  }
}

module leftShoulder()
{
  translate(shoulderPos) {
    rotate(-shoulderAngle, [0,1,0]) {
      translate([0,0,-shoulderHeight/2]) {
        difference() {
          roundedBoxY([shoulderWidth,shoulderWall,shoulderHeight],[0,0,3,3]);
          shoulderHoles();
        }
      }
    }
  }
}

module gauge()
{
  translate([0,-gaugeOutset,0]) {
    rotate(90,[1,0,0]) {
      difference() {
        union() {
          cylinder(r=gaugeR+small,h=gaugeThickness);
          translate([-gaugeR,gaugeR/2,0]) cube([gaugeR*2,gaugeR/2,gaugeThickness]); // support
        }
        translate(gaugeAxlePos) cylinder(r=gaugeAxleR, h=gaugeThickness+small*2);
      }
    }
  }
}

switchOffset = 18; // down from top Z-wise
switchBaseR = 8;
switchPos1 = [0,0,-15];
switchRot1 = 90;
switchPos2 = [0,0,-15];
switchRot2 = -90;
switchThickness = 3;
switchOutset = 23.5;
switchHoleR = 2;

module switch()
{
  rotate(90,[1,0,0]) {
    translate([0,0,switchOutset]) {
      difference() {
        cylinder(r=switchBaseR, h=switchThickness);
        translate([0,0,-small]) cylinder(r=switchHoleR, h=switchThickness+2*small);
      }
    }
  }
}
module switchNegative()
{
  rotate(90,[1,0,0]) {
    translate([0,0,switchOutset]) {
      translate([0,0,-20]) cylinder(r=switchBaseR, h=40);
    }
  }
}


module gaugeNegative()
{
  translate([0,-gaugeOutset,0]) {
    rotate(90,[1,0,0]) {
      translate([0,0,-20]) cylinder(r=gaugeR,h=40);
      translate(gaugePinPos1) cylinder(r=gaugePinR, h=gaugePinH);
      translate(gaugePinPos2) cylinder(r=gaugePinR, h=gaugePinH);
    }
  }
}

module resevoirTop()
{
  translate([0,0,-resevoirCapHeight-small]) {
    difference() {  // ring at top
      cylinder(r=resevoirCapOR, h=resevoirCapHeight+small);
      translate([0,0,-small]) {
        cylinder(r=resevoirCapIR, h=resevoirCapHeight+3*small);
      }
    }
    difference() {  // elliptical bulb
      union() {
        difference() {
          rotate(180,[1,0,0]) elliptical();
          translate([0,0,-resevoirTopZ/2]) gaugeNegative();
          translate(switchPos1) rotate(switchRot1,[0,0,1]) switchNegative();
          translate(switchPos2) rotate(switchRot2,[0,0,1]) switchNegative();
        }
        translate([0,0,-resevoirTopZ/2]) gauge();
        translate(switchPos1) rotate(switchRot1) switch();
        translate(switchPos2) rotate(switchRot2) switch();
      }
      translate([-resevoirOR-small,-resevoirOR-small,-resevoirHeight-small]) {
        cube([resevoirOR*2+small*2,resevoirOR*2+small*2,resevoirHeight-resevoirTopZ+small]);
      }
    }
  }
}

module bottomVolume()
{
   difference() {
      rotate(180,[1,0,0]) hull() elliptical();
      translate([-resevoirOR-small,-resevoirOR-small,-resevoirTopZ]) {
        cube([resevoirOR*2+small*2,resevoirOR*2+small*2,resevoirTopZ+small]);
      }
    }
}

resevoirBottomFloor = 3;
module resevoirBottom()
{
  translate([0,0,-resevoirCapHeight-small]) {
    // tri-axial prop
    intersection() {
      translate([0,0,-resevoirHeight+5]) prop();
      bottomVolume();
    }
    difference() {
      bottomVolume();
      translate([0,0,-resevoirHeight-resevoirBottomFloor]) cylinder(r=18,h=resevoirHeight-resevoirTopZ);
    }
    translate([0,0,-resevoirTopZ-resevoirBottomFloor+small]) rotate(180,[1,0,0]) hull() nose();
    translate([0,0,-resevoirTopZ-resevoirBottomFloor+small]) rotate(180,[1,0,0]) {
      difference() {
          shield();
          cylinder(r1=13,r2=11,h=30);
      }
    }
  }
}

propA = 30;
propR = 100;
propR1 = 0.1;
propPos1 = [-20,0,0];
propR2 = 2.5;
propPos2 = [0,0,0];

module propBlade()
{
  rotate(90-propA,[0,1,0]) {
    linear_extrude(height=propR, center=false, convexity=10, twist=0) {
      hull() {
        translate(propPos1) circle(r=propR1);
        translate(propPos2) circle(r=propR2);
      }
    }
  }
}

module prop()
{
  intersection() {
    union() {
      propBlade();
      rotate(120,[0,0,1]) propBlade();
      rotate(240,[0,0,1]) propBlade();
    }
  }
}

// base
//base();

// ring
//ring();

// back
//belt();
//shoulder();

resevoirTop();
//resevoirBottom();

//bottle();

