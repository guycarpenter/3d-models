#!/usr/bin/env python

from math import cos,sin,pi,sqrt
from openscad import *

# prolate hemispheroid
def elliptical(poly, length, radius, xSteps, aSteps, outer):

    pOrg = len(poly.points)
    origin = [0,0,length]
    poly.points.append(origin)
    for i in range(1, xSteps+1):
        z = i * length / xSteps
        p0 = len(poly.points)
        
        for j in range(0, aSteps):
            a = j * 360.0 / aSteps
            aRad = a / 180.0 * pi
            zPrime = length - z
            r = radius * sqrt(1-(zPrime*zPrime)/(length*length))
            x = r * sin(aRad)
            y = r * cos(aRad)
            poly.points.append([x,y,zPrime])

            if outer:
                a = p0-aSteps+(j+1)%aSteps
                b = p0-aSteps+j
                c = p0+(j+1)%aSteps
                d = p0+j
            else:
                a = p0-aSteps+j
                b = p0-aSteps+(j+1)%aSteps
                c = p0+j
                d = p0+(j+1)%aSteps
            if (i==1):
                poly.triangles.append([pOrg,d,c])
            else:
                poly.triangles.append([a,b,c])
                poly.triangles.append([c,b,d])
                
def cap(poly, xSteps, aSteps):
    p0 = 1 + aSteps * (xSteps-1)
    p1 = 1 + aSteps * xSteps + p0
    
    for j in range(0, aSteps):

        a = p0+j
        b = p0+(j+1)%aSteps
        c = p1+j
        d = p1+(j+1)%aSteps
        
        poly.triangles.append([c,b,a])
        poly.triangles.append([b,c,d])


def openscadElliptical(module, length, radius, xSteps, aSteps, wall):
    poly = OpenscadPolyhedron()
    poly.convexity = 2 # max front faces penetrated by any ray
    elliptical(poly, length, radius, xSteps, aSteps, True)
    elliptical(poly, length-wall, radius-wall, xSteps, aSteps, False)
    cap(poly, xSteps, aSteps)
    scadFile = open(module+".scad", "w")
    scadFile.write("module "+module+"() {\n");
    poly.write(scadFile)
    scadFile.write("\n}\n"+module+"();\n");

bottleD = 53.5
baseWall = 3.0

openscadElliptical("elliptical", 50.0, bottleD/2 + baseWall, 20, 32, 5)
openscadElliptical("nose", 15.0, 10.0, 20, 32, 4)
openscadElliptical("shield", 15.0, 16.0, 20, 32, 4)
