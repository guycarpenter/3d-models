small = 0.1;

strapWidth = 50.0;
wallWidth = 6.0;
thickness = 4.0;

centerHeight = wallWidth*2;
centerThickness = 3.0;

holeWidth = strapWidth + 4;
holeHeight = 8.0 * 2 + centerHeight;

centerSize = [holeWidth+2*small, centerHeight, centerThickness];
centerPos = [wallWidth-small*2,wallWidth+8,0];
holeSize = [holeWidth, holeHeight*2+centerHeightidth, thickness+small*2];
holePos = [wallWidth,wallWidth,-small];

buckleSize = [holeWidth+2*wallWidth, holeHeight+2*wallWidth, thickness];

holeSize = [holeWidth, holeHeight, thickness+2*small];
holePos1 = [wallWidth,wallWidth,-small];
holePos2 = [wallWidth,wallWidth*2+holeHeight,-small];

module buckle()
{
  difference() 
  {
    cube(buckleSize);
    translate(holePos) cube(holeSize);
  }
  translate(centerPos) cube(centerSize);
}

buckle();


