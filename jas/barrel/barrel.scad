$fn = 64;

small = 0.1;
length = 100.0;
outerD = 31.86;
wallThickness = 4.2;
innerD = outerD - wallThickness * 2;
tipLength = 7.0;
mountLength = 4.0;

module mount()
{
  cylinder(h=mountLength+small, r1=outerD/2-2.86, r2=outerD/2);
}

module barrel()
{
  difference() {
    union() {
      translate([0,0,tipLength]) cylinder(h=length-tipLength, r=outerD/2);
      cylinder(h=tipLength, r1=outerD/2-2.86, r2=outerD/2);
    }
    translate([0,0,-small]) {
      cylinder(h=length + small * 2, r=innerD/2);
    }
    translate([0,0,length-mountLength]) mount();
    grooves();
  }
}

notchThickness = 2.5;
notchDepth = 1.5;
notchLength = 33;
notches = 13;

module grooves()
{
  for (i = [0 : 360/notches : 360] )
  {
    rotate (i, [0,0,1]) {
      translate([outerD/2 - notchDepth, -notchThickness/2, 0]) {
        cube([notchDepth+small, notchThickness, notchLength]);
      }
    }
  }
}

barrel();
