

union()
	{
	translate([0,-7.5,0]) 
		cube(size=[15,15,3]);

	translate([0,-3,-5]) 
		cube(size=[15,3,8]);

	translate([0,0,-5]) 
		cube(size=[15,13,3]);

	translate([0,13,-5]) 
		rotate (a=[45,0,0]) 
			cube(size=[15,3,3]);

	translate([0,-41.3,27.2]) 
		rotate (a=[-40,0,0]) 
			cube(size=[15,50,4.5]);
	}