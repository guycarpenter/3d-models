monacleIR = 21.58/2;
monacleOR = monacleIR + 3;
monacleH = 3.0;
monacleFn = 120;
mountOR = 5.0;
mountIR = 2.0;

//mountIR = 2.0;
//mountOR = 5.0;
monacleMountPos = [monacleOR+mountOR-mountIR,0,0];

module monacleMount()
{
  translate(monacleMountPos) {
    cylinder(r=mountOR, h=monacleH, $fn=monacleFn);
  }
}

module monacleMountNegative()
{
  translate(monacleMountPos) {
    cylinder(r=mountIR, h=monacleH, $fn=monacleFn);
  }
}


module monacle()
{
  difference() {
    hull() {
      cylinder(r=monacleOR, h=monacleH, $fn=monacleFn);
      monacleMount();
    }
    cylinder(r=monacleIR, h=monacleH, $fn=monacleFn);
    monacleMountNegative();
  }
}

monacle();
