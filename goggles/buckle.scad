small = 0.1;

strapWidth = 25.0;
wallWidth = 3.0;
thickness = 2.0;

holeWidth = strapWidth;
holeHeight = 4.0;
holeSize = [holeWidth, holeHeight, thickness+small*2];

buckleSize = [holeWidth+2*wallWidth, holeHeight*2+3*wallWidth, thickness];

holePos1 = [wallWidth,wallWidth,-small];
holePos2 = [wallWidth,wallWidth*2+holeHeight,-small];

module buckle()
{
  difference() 
  {
    cube(buckleSize);
    translate(holePos1) cube(holeSize);
    translate(holePos2) cube(holeSize);
  }
}

buckle();


