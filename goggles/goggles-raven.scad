small = 0.1;
innerR = 52.0 / 2;
outerR = 56.0 / 2;
lowH = 12.0;
highH = 40.0;

trimH = 3.0;
trimR = 57.0 / 2;
trimPos = [0,0,0];
trimSteps = 12;
trimWidth = 8;

$fn = 16;
highQ = 128;

truncateR = 80;

//  H        C
//  |\
//  |  M
//  |    \ L
//  |      |
//  ---D----

outerD = outerR * 2.0;
dv = highH - lowH;

distHL = sqrt(outerD*outerD + dv*dv);

angleL = atan(dv/outerD);

halfHL = distHL/2.0;
distMC = sqrt(truncateR*truncateR - halfHL*halfHL);

truncateCY = distMC * sin(angleL);
truncateCZ = distMC * cos(angleL) + (lowH+highH)/2;
truncatePos = [0,truncateCY,truncateCZ];

scoopR1 = 15.0;
scoopScale1 = [1.0,1.0,0.8];
scoopPos1 = [0,0,-4];

scoopR2 = 1.0;
scoopScale2 = [1,1,1];
scoopPos2 = [20,0,5];

clipHoleWidth = 22.0;
clipHoleHeight = 4.0;
clipPos1 = [0,-outerR,15];
clipPos2 = [0,-outerR,25];

ringR1 = outerR + 1.5;
ringR2 = ringR1 + 1.0;
ringH  = 7.0;
ringZ  = 5.0;  // probaby want this same as spikeZ
ringPos = [0,0,ringZ-ringH/2];

module ring()
{
  translate(ringPos) 
  {
    difference()
    {
      cylinder(r=ringR2, h=ringH, $fn=highQ);
      translate([0,0,-small]) cylinder(r=ringR1, h=ringH+small*2, $fn=highQ);
    }
  }
}

module scoop(scale)
{
  scale([scale,scale,scale])
  {
    hull()
    {
      intersection()
      {
        //translate([0,0,50]) cube([100,100,100], center=true);
        union() 
        { 
          translate(scoopPos1) scale(scoopScale1) sphere(r = scoopR1, $fn=30);
          translate(scoopPos2) scale(scoopScale2) sphere(r = scoopR2);
        }
      }
    }
  }
}

module trim1()
{
  for (i = [1:trimSteps-1]) {
    rotate([0,0,360.0/trimSteps*i]) {
      translate([0,-outerR,7]) rotate([0,270,90]) scoop(0.5);
    }
  }
  translate([0,-outerR,15]) rotate([0,270,90]) scoop(1.1);
}

spikeR1 = 3.0;
spikeR2 = 1.0;
spikeH  = 5.0;
spikeZ  = 5.0;
spikePos = [0,0,-0.5];  // avoid gaps sitting on outer radius
module spike()
{
  translate(spikePos)
    cylinder(r1=spikeR1, r2=spikeR2, h=spikeH);
}

mount1Pos = [0,0,20];
mount2Pos = [0,0,5];
mountOR = 5.0;
mountIR = 2.0;
mountH = 4;
mountA = 225;  // -45 for right, 225 for left
mountUp = 5;

module mounts()
{
  translate(mount1Pos) mount();
  translate(mount2Pos) mount();
}

module mount()
{
  rotate([0,0,mountA])
  {
    translate([outerR+mountUp,0,0]) {
      difference() {
        union() {
          cylinder(r=mountOR, h=mountH);
          translate([-mountOR-mountUp,-mountOR,0])
            cube([mountOR+mountUp,mountOR*2,mountH]);
        }
        cylinder(r=mountIR, h=mountH+small);
      }
    }
  }
}

flourishH = 3.0;
flourishR1 = 5.0;
flourishR2 = 3.0;
flourishPos1 = [-1,4,-flourishH/2];
flourishPos2 = [-1,10,-flourishH/2];

module flourish()
{
  translate([outerR,0,0]) {
    rotate([90,0,0]) {
      hull() {
        translate(flourishPos1) cylinder(r=flourishR1, h=flourishH);
        translate(flourishPos2) cylinder(r=flourishR2, h=flourishH);
      }
    }
  }
}

module trim()
{
  // scoop covering clips
  // REMOVED FOR RAVEN
  //translate([0,-outerR,15]) rotate([0,270,90]) scoop(1.1);

  for (i = [1:trimSteps]) {
    rotate([0,0,270+360.0/trimSteps*i]) {
      flourish();
    }
  }
  
}


module clipNegative()
{
  intersection() 
  {
    translate([0,0,clipPos1[2]-clipHoleHeight/2]) cylinder(r=ringR1, h=100, $fn=highQ);
    union() 
    {
      translate(clipPos1) cube([clipHoleWidth,20,clipHoleHeight],center=true);
      translate(clipPos2) cube([clipHoleWidth,20,clipHoleHeight],center=true);
      /* RAVEN
      difference() {
        translate([0,-50,0]) cube([clipHoleWidth, 100, 100], center=true);
        cylinder(r=outerR, h=100, $fn=highQ);
      }
      */
    }
  }
}

module trim0()
{
  translate(trimPos) {
    difference() {
      cylinder(h=trimH, r=trimR);
      for (i = [1:trimSteps]) {
        rotate([0,0,360.0/trimSteps*i]) {
          cube([100,trimWidth,100], center=true);
        }
      }
    }
  }
}


module goggles()
{
  difference() 
  {
    union()
    {
      trim();
      //mounts();
      difference()
      {
        union() {
          cylinder(h=highH, r=outerR, $fn=highQ);
          //trim();
          ring();
        }
        
        translate([0,0,-50]) cube([100,100,100], center=true);
        translate(truncatePos) {
          rotate([0,90,0]) {
            cylinder(h=100, r=truncateR, center=true, $fn=highQ);
          }
        }
      }
    }
    clipNegative();
    // remove inside cylinder
    translate([0,0,-small]) cylinder(h=highH*2, r=innerR, $fn=highQ, center=true);
    // ensure front is flat.
    translate([0,0,-50]) cube([100,100,100], center=true);
  }
}


goggles();

//include <monacle.scad>
//rotate([0,0,mountA])
//  translate ([outerR-monacleMountPos[0]+mountUp,0,-5]) 
//    monacle();

