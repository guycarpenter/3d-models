
// stays are 3mm wide.  At the radius of arcIR,
// how many degrees is that?
// C = 2 * pi * R
// C = 351.9
// so 3mm is about 3 degrees.
// There are 12 x 30 degrees in 360
// stay takes up 3, leave 30-3=27 for the gap between the stays.
// try 25 degrees.

arcA0 = 3;
arcA1 = arcA0 + 23;

arcIR = 56.0 / 2;
arcOR = arcIR + 1.2;
arcH = 8.0;

diverge = [20,0,0];
divergeAdjust = arcIR * -sin(diverge[0]);
arcPos = [0,-arcOR-2,divergeAdjust];

$fn = 120;

small = 0.1;

module arc()
{
  translate(arcPos) {
    rotate(diverge)
      difference() {
        cylinder(r=arcOR, h=arcH, center=false);
        rotate([0,0,arcA0]) translate([0,-50,-small]) cube([100,100,arcH+2*small], center=false);
        rotate([0,0,-180+arcA1]) translate([0,-50,-small]) cube([100,100,arcH+2*small], center=false);
      }
  }
}

module arcNegative()
{
  translate(arcPos) {
    rotate(diverge)
      cylinder(r=arcIR, h=100, center=true);
  }
  translate([0,0,-50]) cube([100,100,100], center=true);
}

plateOR = 20;
plateIR = plateOR-5;
plateH  = 3;
module plate()
{
  translate([plateIR-10,0,0]) {
    difference() {
      cylinder(r=plateOR, h=plateH);
      translate([0,0,-small]) cylinder(r=plateIR, h=plateH+small*2);
    }
  }
}

arc1Pos = [0,0,0];
arc2Pos = [0,10,0];
module noseClip()
{
  difference() {
    union() {
      arc();
      mirror([0,1,0]) arc();
      plate();
    }
    arcNegative();
    mirror([0,1,0]) arcNegative();
    translate([0,-50,0]) cube([100,100,100]);
  }
}

noseClip();
