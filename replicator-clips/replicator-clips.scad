plyThickness = 5.7;
coverThickness = 2.2;

diskR = 8.0;
diskH = 5.0;
catchH = diskH-(plyThickness-coverThickness)/2;
catchOffset = 8.0;

holeR = 2.0;
sinkR = 4.0;
small = 0.1;

module disk()
{
  translate([0,0,diskH/2]) {
    cylinder(h=diskH, r=diskR, center=true);
  }
}

module hole(sink)
{
  translate([0,0,diskH/2]) {
    cylinder(h=diskH+small, r=holeR, center=true);
  }
  if (sink) {
    translate([0,0,(sinkR-holeR)/2]) {
      cylinder(h=sinkR-holeR+small, r1=sinkR, r2=holeR, center=true);
    } 
  }
}

module catch()
{

  translate([0,0,catchH/2]) {
    hull() {
      cylinder(h=catchH, r=diskR, center=true);
      translate([catchOffset,0,0]) {
        cylinder(h=catchH, r=diskR, center=true);
      }
    }
  }
}

module clip(sink)
{
  difference()
  {
    union() {
      disk();
      catch();
    }
    hole(sink);
  }
}

module pair()
{
  clip(true);
  translate([0,diskR*2+3.0,0]) {
    clip(false);
  }
}

module twoPair()
{
  pair();
  translate([0,diskR*4+6.0,0]) {
    pair();
  }
}

module fourPair()
{
  twoPair();
  translate([-diskR*2-3,0,0]) {
    mirror([1,0,0]) {
      twoPair();
    }
  }
}

fourPair();
