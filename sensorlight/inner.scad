radiusTop = 17.5/2;
radiusBottom = 17.6/2;
height = 2;

wireHoleRadius = 1.5;
wireHoleOffset = 5.0;
ledHoleRadius = 3;

small = 0.5;

module cap()
{
  cylinder(h=height,r1=radiusBottom,r2=radiusTop);
}

module void()
{
  translate([0,0,-small/2]) 
  {
    translate([wireHoleOffset,0,0])
      cylinder(h=height+small, r=wireHoleRadius);
    translate([-wireHoleOffset,0,0])
      cylinder(h=height+small, r=wireHoleRadius);
      cylinder(h=height+small, r=ledHoleRadius);
  }
}

difference()
{
  cap();
  void();
}
