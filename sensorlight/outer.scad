
height = 7;
innerRadiusTop = 25/2;
innerRadiusBottom = 24/2;
sleeveThickness = 2;
topHeight = 2;
washerLap = 5;
small = 0.1;
$fa = 1;
module sleeve()
{
  cylinder(h=height,r1=innerRadiusBottom+sleeveThickness,r2=innerRadiusTop+sleeveThickness);
}

module washer()
{
  cylinder(h=topHeight,r1=innerRadiusBottom+washerLap,r2=innerRadiusBottom+washerLap+topHeight);
}

module void()
{
    translate([0,0,-small/2])
      cylinder(h=height+small,r1=innerRadiusBottom,r2=innerRadiusTop);
}

difference()
{
  union() 
  {
    sleeve();
    washer();
  }
  void();
}

