guideHeight = 10;
slotDepth = 2.0;
slotWidth = 1.8;
guideWidth = 2+slotWidth+2;
guideDepth = slotDepth+2;
small = 0.1;
guideSeparation = 54+1-0.5; // 1mm slack
$fn=64;


slabWidth = 50.0 + 2.0;
slabHeight = 2.0;
slabDepth = guideSeparation + 10;

slabSize = [guideSeparation + 10, slabWidth, 2];
slabPosition = [-slabDepth/2-guideSeparation/2, -10, -slabHeight];


module boxCornerNegative(centre, size, rotation)
{
  translate(centre) {
    rotate([0,0,rotation])
    {
      difference()
      {
        translate([-small,-small,-small/2]) {
          cube([size[0]+small,size[1]+small,size[2]+small]);
        }
        translate([size[0],size[1],-small]) {
          cylinder(h=size[2]+small*2,r=size[0], center=false);
        }
      }
    }
  }
}

module roundedBox(size, radii)
{
  difference()
  {
    cube(size);
    boxCornerNegative([0,0,0],[radii[0],radii[0],size[2]],0);
    boxCornerNegative([0,size[1],0],[radii[1],radii[1],size[2]],270);
    boxCornerNegative([size[0],0,0],[radii[2],radii[2],size[2]],90);
    boxCornerNegative([size[0],size[1],0],[radii[3],radii[3],size[2]],180);
  }
}

/*      
 *   Y  /\
 *     |\ \
 *     | \ \
 *   Z |  \ \
 *     |   \/
 *     +----
 *       X
 */
module brace(size)
{
  polyhedron (
    points = [
      [0, 0, size[2]],
      [0, size[1], size[2]], 
      [0, size[1], 0], 
      [0, 0, 0],
      [size[0], 0, 0],
      [size[0], size[1], 0]
    ],
    triangles = [[0,3,2],
                 [0,2,1],
                 [3,0,4],
                 [1,2,5], 
                 [0,5,4],
                 [0,1,5],
                 [5,2,4],
                 [4,2,3],
   ]
  );
}

module guide()
{
  translate([-slotDepth,-guideWidth/2,0]) {
    difference()
    {
      union() {
        cube([guideDepth,guideWidth,guideHeight]);
        translate([guideDepth,guideWidth-small,-small])
          rotate([0,0,90])
            brace([guideFlare,guideDepth,guideHeight]);
        translate([0,small,0])
          rotate([0,0,-90])
            brace([guideFlare,guideDepth,guideHeight]);
      }
      translate([-small,(guideWidth-slotWidth)/2,-small]) {
        cube([slotDepth+small,slotWidth,guideHeight+small*2]);
      }
    }
  }
}
guideFlare=4;
module guides()
{
  guide();
  translate([-guideSeparation,0,0]) 
    rotate([0,0,180]) 
      guide();
}

holeSize = [48,12,slabHeight+small*2];
holeInset = 7;
holePosition = [slabSize[0]/2-holeSize[0]/2,slabSize[1]-holeSize[1]-holeInset,-small];

module slab()
{
  translate(slabPosition)
    difference() {
      roundedBox(slabSize,[4,4,4,4]);
      translate(holePosition)
        roundedBox(holeSize,[2,2,2,2]);
    }
}

cornerThickness = 2.0;
cornerFlat = 2.0;
cornerFlare = 4.0;
cornerHeight = 6.0;
cornerSeparationX = 54.0;

corner1Position = [slabSize[0]/2-cornerSeparationX/2,slabSize[1]-cornerThickness,slabSize[2]];
corner2Position = [slabSize[0]/2+cornerSeparationX/2,slabSize[1]-cornerThickness,slabSize[2]];

module cornerWall()
{
  translate([-cornerThickness,-cornerThickness,0]) {
    cube([cornerFlat+cornerThickness+small, cornerThickness, cornerHeight]);  // across back
  }
  translate([cornerFlat,-cornerThickness,0]) {
    brace([cornerFlare,cornerThickness,cornerHeight]);
  }
}

module corner()
{
  cornerWall();
  translate([-cornerThickness,0,0]) rotate(a=[0,0,90]) cornerWall();
}

module corners()
{
  translate(slabPosition) {
    translate(corner1Position) {
      rotate([0,0,270]) {
        corner();
      }
    }
    translate(corner2Position) {
      rotate([0,0,180]) {
        corner();
      }
    }
  }
}

corners();
guides();
slab();
