include <BoxParameters.scad>

// Start by creating the basic shape, then removing all of the spaces needed
difference()
{
	// Base shape
	cube([overallL, overallW, overallH - top]);
	
	// Remove the large middle section for batteries/electronics
	translate([wall, wall, bottom]) 
		cube([innerL, innerW, innerH + top]);

	// Remove space for the USB hole
	translate([-1, wall + usbFromWall, bottom + usbFromFloor]) 
		cube([usbL, usbW, usbH]);

	if(!ingressBox) {
		// Remove circular space for the status LED
		translate([-1, wall + ledFromWall, bottom + ledFromFloor])
			rotate([0, 90, 0])
				cylinder(5, ledCirc, ledCirc);
	} else {
		// Put a resistance logo shape for the status LED space
		translate([0, overallW / 3, overallH / 2 + 1]) // Plus 1 to pad it up a bit
			rotate([0, 90, 0])
				scale([0.15, 0.15, 6])
					import("ResistanceLogoForHole.stl");
	}

	// Make space for the switch
	translate([-1, wall + switchFromWall, bottom + switchFromFloor]) 
		cube([switchL, switchW, switchInH]);

	// Make space for some lid clips
	translate([wall - clipW, innerW * (2/3) + wall, overallH - top - clipFromTop - clipH])
		cube([clipW + padding, innerW / 4, clipH]);

	translate([overallL - wall - padding, wall * 2, overallH - top - clipFromTop - clipH])
		cube([clipW + padding, innerW / 4, clipH]);

	// Attempt to make it pretty by rounding the corners
	// - Four base edges
	translate([0, 0, -4])
		rotate([45, 0, 0])
			translate([0,0,1])
				cube([innerL + wall*2, 4, 4]);
	translate([0, innerW + wall*2, -4])
		rotate([45, 0, 0])
			translate([0,1,0])
				cube([innerL + wall*2, 4, 4]);
	translate([0, 0, -4])
		rotate([45, 0, 90])
			translate([0,1,0])
				cube([innerL + wall*2, 4, 4]);
	translate([innerL + wall*2, 0, -4])
		rotate([45, 0, 90])
			translate([0,0,1])
				cube([innerL + wall*2, 4, 4]);
	// - Four side edges
	translate([-4, 0, innerH + bottom + top])
		rotate([45, 90, 0])
			translate([0,0,1])
				cube([innerL + wall*2, 4, 4]);
	translate([-4, innerW + wall*2, innerH + bottom + top])
		rotate([45, 90, 0])
			translate([0,1,0])
				cube([innerL + wall*2, 4, 4]);
	translate([innerL + wall*2 - 1.5, 0, innerH + bottom + top])
		rotate([45, 90, 0])
			translate([0,-1,0])
				cube([innerL + wall*2, 4, 4]);
	translate([innerL + wall*2 - 1.5, innerW + wall*2, innerH + bottom + top])
		rotate([45, 90, 0])
			translate([0,0,-1])
				cube([innerL + wall*2, 4, 4]);
}
	

// battery separators
module separator(x, y, z, degx) {
	difference() {
		translate([x, y, z])
			rotate([degx,270,0])
				linear_extrude(cellWall)
					polygon([[0,0],[0, (cellL / 2 - 10)],[(cellW - 2), 0]],[[0,1,2]]);

		// Remove space for terminals
		if(degx == 180) {
			translate([x - 0.5, y - cellWall, z + 4])
				cube([terminalSpace + 1.5, cellWall + padding * 2, terminalSpace]);
		} else {
			translate([x - 1.5, y, z])
				cube([terminalSpace, cellWall + padding * 2, terminalSpace + 4]);
		}
	}
}

for(i = [1:4])
	separator(innerL + wall - (cellW + cellWall) * i, innerW + wall + padding, bottom, 180);

for(i = [1:4])
	separator(innerL + wall - (cellW + cellWall) * i + cellWall, wall, bottom, 0);