include <BoxParameters.scad>

// build top peice
difference() {
	translate([0, 0, topInsert])
		cube([overallL, overallW, top]);
	
	if(ingressBox) {
		translate([overallL / 2, overallW / 2, topInsert + (top / 2)])
			scale([15, 15, 1])
				linear_extrude(top)
					import("Ingresslogo.DXF");
	}

	// Attempt to make it pretty by rounding the corners
	// - Four top edges
	translate([0, 0, topInsert])
		rotate([45, 0, 0])
			cube([innerL + wall*2, 4, 4]);
	translate([0, innerW + wall*2, topInsert])
		rotate([45, 0, 0])
			cube([innerL + wall*2, 4, 4]);
	translate([0, 0, topInsert])
		rotate([45, 0, 90])
			cube([innerL + wall*2, 4, 4]);
	translate([innerL + wall*2, 0, topInsert])
		rotate([45, 0, 90])
			cube([innerL + wall*2, 4, 4]);

	// Cut out a peice to help open
	translate([innerL, wall * 2, topInsert])
		cube([5, innerW / 4 - 2, top / 2]);
}

// Build insert
difference() {
	translate([wall + padding, wall + padding, 0])
		cube([innerL - padding, innerW - padding, topInsert]);

	translate([wall, wall, -padding])
		cube([usbL, usbW + wall + usbFromWall + 1, topInsert + padding]);
}

// Clips
translate([wall - clipW, innerW * (2/3) + wall * 2, padding])
	cube([clipW + padding, innerW / 4 - wall * 3, clipH - padding * 2]);

translate([overallL - wall - padding, wall * 3, padding])
	cube([clipW + padding, innerW / 4 - wall * 3, clipH - padding * 2]);