// Component Specifics
cellL = 54; // Length for single battery holder
cellW = 14.5; // Width for single battery holder
cellWall = 1; // Wall thickness between batteries

usbL = 15;
usbW = 6;
usbH = 14;
usbFromFloor = 4; // How far the usb hole is from the floor
usbFromWall = 2; // How far the usb hole is from the wall

ledL = 5;
ledCirc = 3.1;
ledFromWall = 14;
ledFromFloor = 11;

elecDepth = 20; // Depth of the compartment for the electronics

wireW = 3; // Width of wire running inside the case

terminalSpace = 2;

clipW = 1;
clipH = 1;
clipFromTop = 1;

switchInH = 11.5;
switchOutH = 19.2;
switchW = 6;
switchL = 8;

// Base box properties
wall = 2;
top = 2;
topInsert = 2; // Set in peice of the lid to make it sit neatly
bottom = 2;
padding = 0.05; // Pad out things a little bit to make sure they fit
innerL = (cellW + cellWall) * 4 + elecDepth;
innerW = cellL;
innerH = cellW + 3; // Plus 3 for some extra space to fit usb port

overallL = innerL + wall*2;
overallW = innerW + wall*2;
overallH = innerH + bottom + top;

ingressBox = true; // Determines if the box is customised for ingress

switchFromWall = innerW - 10;
switchFromFloor = (innerH - switchInH) / 2 - 0.5;