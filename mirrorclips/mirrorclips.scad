// hole diam 2mm
// sink diam 4mm
// sink depth 1mm
// bigDiam 8mm
// smallDiam  5mm
// length    14mm

small       =  1.0;
thickness   =  2.0;
holeDiam    =  2.0;
sinkDiam    =  4.0;
sinkDepth   =  1.0;
diamA       =  8.0;
diamB       =  5.0;
length      = 14.0;

module body()
{
  hull() {
    cylinder(h=thickness, r=diamA/2, $fa=1, center=true);
    translate([length, 0,0]) {
      cylinder(h=thickness, r=diamB/2, $fa=1, center=true);
    }
  }
}

module sink()
{
  translate([0,0,thickness/2-sinkDepth]) {
    cylinder(h=sinkDepth+small, r=sinkDiam/2, $fa=1);
  }
}

module hole()
{
  cylinder(h=thickness+2*small, r=holeDiam/2, $fa=1, center=true);
}

module clip()
{
  difference() {
    body();
    hole();
    sink();
  }
}

translate([0, 0,0]) { clip(); }
translate([0,10,0]) { clip(); }
translate([0,20,0]) { clip(); }
translate([0,30,0]) { clip(); }


  
