height       = 5;
innerRadius  = 20;
outerRadius  = 32;
flangeWidth  = 2;
flangeHeight = 2;

notchBase    = 8;
notchHeight  = 4;
notchCount   = 10;
notchLength  = outerRadius+flangeWidth+1;


module ringNegative()
{
  translate([0,0,height])
  {
    difference() {
      cylinder(h=flangeHeight+1, r=outerRadius);
      cylinder(h=flangeHeight+2, r=innerRadius);
    }
  }
}


module ringPositive()
{
  translate([0,0,0])
  {
    difference() {
      cylinder(h=height+flangeHeight, r=outerRadius+flangeWidth);
      cylinder(h=height+flangeHeight+1, r=innerRadius-flangeWidth);
    }
  }
}

module notch()
{
  polyhedron ( points = [[-notchBase/2, 0, 0],
                         [0, 0, notchHeight],
                         [ notchBase/2, 0, 0], 
		                  [-notchBase/2, notchLength, 0],
                         [0, notchLength, notchHeight],
                         [ notchBase/2, notchLength, 0]
                        ],
              triangles = [
                            [2,1,0],[3,4,5],
                            [0,1,3],[1,4,3],
                            [0,3,2],[2,3,5],
                            [1,2,4],[4,2,5],
           
                          ]);
}

module notches()
{
  for (i = [0 : notchCount])
  {
    rotate(i * 360 / notchCount, [0,0,1])
      notch();
  }   
}

module bottomNotches()
{
  notches();
}

module topNotches()
{
  rotate(0.5 * 360 / notchCount, [0,0,1])
  {
    translate([0,0,height+flangeHeight])
    {
      mirror([0,0,1]) {
        notches();
      }
    }
  }
}

difference() {
 ringPositive();
 ringNegative();
 topNotches();
 bottomNotches();
}

