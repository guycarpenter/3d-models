small = 0.01;

// first sample was 1.4 wall, 20x20 => 22.8
// with 2.0 wall, same od with 18.8x18.8 => 22.8

boxIL = 18.8;
boxIW = 18.8;
boxIH = 10;
boxOR = 2.0;
boxIR = 0.0;

wallThickness = 2.0;
floorThickness = 1.0;

boxOL = boxIL + 2*wallThickness;
boxOW = boxIW + 2*wallThickness;
boxOH = boxIH + floorThickness;

lidMargin = 2.5;
lidIL = boxOL-lidMargin*2;
lidIW = boxOW-lidMargin*2;
lidH  = 2.0;

flangeHeight = 5;
flangeThickness = 0.7;
flangeAllowance = 0.3; // fitting allowance
bezelHeight = 0.7;

module lid()
{
}

module box_body()
{
  difference()
  {
    cube([boxOL, boxOW, boxOH]);
    translate([wallThickness, wallThickness, floorThickness]) 
    {
      cube([boxIL,boxIW,boxIH+small]);
    }
  }
}

module square_sleeve(length,width,height,thickness)
{
  difference()
  {
    cube([length,width,height]);
    translate([thickness,thickness,-small]) 
    {
      cube([length-thickness*2.0,width-thickness*2.0,height+small*2.0]);
    }
  }
}

module square_sleeve_rounded(length,width,height,thickness,outerradius,innerradius)
{
  difference()
  {
    square_sleeve(length,width,height,thickness);

  }
}

module box_recess()
{
  translate([-small,-small,boxOH-flangeHeight]) 
  {
    square_sleeve(boxOL+small*2,boxOW+small*2,flangeHeight+small*2,flangeThickness+flangeAllowance+small);
  }
}


module lid_top()
{
  translate([0,0,boxOH+bezelHeight]) 
  {
    difference()
    {
      cube([boxOL,boxOW,lidH]);
      translate([lidMargin,lidMargin,-small]) 
      {
        cube([lidIL,lidIW,lidH+2*small]);
      }
    }
    translate([0,0,-flangeHeight-bezelHeight])
    { 
      square_sleeve(boxOL,boxOW,flangeHeight+bezelHeight,flangeThickness);
    }
  }
}

module box()
{
  difference()
  {
    box_body();
    box_recess();
  }
}

module lid()
{
  lid_top();
}

// print both parts for testing

box();
translate([0,0,10])
{
  lid();
}
/*
translate([0,-10,boxOH+lidH+bezelHeight])
{
  rotate([180,0,0]) 
  {
    lid();
  }
}
*/
