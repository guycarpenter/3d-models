small = 0.1;
highQ=360;
lowQ=60;

grooveR = 10.5/2;

// height is to center of arc
mountHeight = 35.0;
mountHeightExtra = grooveR/2;
mountWidth = 23.7;
mountDepth = 10;
mountSize = [mountWidth,mountDepth,mountHeight+mountHeightExtra];
mountPos = [-mountWidth/2,-mountDepth/2,0];

groovePos = [0,0,mountHeight+grooveR];

module cornerNegative(centre, size, rotation)
{
  translate(centre) {
    rotate([0,0,rotation])
    {
      difference()
      {
        translate([-small,-small,-small/2]) {
          cube([size[0]+small,size[1]+small,size[2]+small]);
        }
        translate([size[0],size[1],-small]) {
          cylinder(h=size[2]+small*2,r=size[0], center=false);
        }
      }
    }
  }
}

module groove()
{
  translate(groovePos)
    translate([-mountWidth/2-small,0,0]) 
      rotate([0,90,0])
        cylinder(r=grooveR, h=mountWidth+small*2, $fn=lowQ);
}

module mountPositive()
{
  translate(mountPos)
    cube(mountSize);
}

sideArcR = 50;
sideArcInset = 3;
module sideArc()
{
  offset = sideArcR+mountWidth/2-sideArcInset;
  translate([offset,mountDepth/2+small,mountHeight/2])
    rotate([90,0,0]) 
      cylinder(r=sideArcR, h=mountDepth+small*2, $fn=highQ);
  translate([-offset,mountDepth/2+small,mountHeight/2])
    rotate([90,0,0]) 
      cylinder(r=sideArcR, h=mountDepth+small*2, $fn=highQ);
}
frontArcR = 100;
frontArcInset = 1.459;
module frontArc()
{
  offset = frontArcR+mountDepth/2-frontArcInset;
  translate([-mountWidth/2-small,offset,mountHeight/2])
    rotate([0,90,0]) 
      cylinder(r=frontArcR, h=mountWidth+small*2, $fn=highQ);
  translate([-mountWidth/2-small,-offset,mountHeight/2])
    rotate([0,90,0]) 
      cylinder(r=frontArcR, h=mountWidth+small*2, $fn=highQ);
}

module mountNegative()
{
  groove();
  frontArc();
  sideArc();
}

module mount()
{
  difference() {
    mountPositive();
    mountNegative();
  }
}

mount();
