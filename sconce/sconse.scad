outerRadius = 252/2;
outerHeight =  10;
rebateDepth = 1;
innerRadius = outerRadius - 5;
innerHeight = outerHeight - rebateDepth;
rebateRadius = outerRadius - 2;

topWidth = 5;

stayDown = 15;
stayOut = 142/2;
stayOuterRadius = 5;
stayInnerRadius = 2.5;
stayWidth = stayOuterRadius*2;
stayHeight = stayDown-topWidth;

vertWidth = 10;
vertLength = innerRadius;
vertDepth = 3;

small = 0.1;

module base()
{
  difference()
  {
    cylinder(h=outerHeight, r=outerRadius, center=true, $fa=1);
    cylinder(h=outerHeight+small, r=innerRadius, center=true, $fa=1);
    translate([0,0,outerHeight-rebateDepth])
      cylinder(h=outerHeight, r=rebateRadius, center=true, $fa=1);
    translate([outerRadius,0,0])
      cube(size=[outerRadius*2,outerRadius*2,outerHeight+small], center=true);
  }
}

module top()
{
  translate([-topWidth/2,0,-rebateDepth/2])
    cube(size=[topWidth,outerRadius*2,outerHeight-rebateDepth], center=true);
}

module vert()
{
  translate([-vertLength/2,0,-outerHeight/2+vertDepth/2])
    cube(size=[vertLength,vertWidth,vertDepth], center=true);
}

module stay()
{
  translate([-stayDown,stayOut,-rebateDepth/2])
  {
    difference() 
    {
      union(){
        translate([stayOuterRadius,0,0]) 
          cube(size=[stayHeight, stayWidth, innerHeight], center=true);
        cylinder(h=innerHeight, r=stayOuterRadius, center=true);
      }
      cylinder(h=innerHeight+small, r=stayInnerRadius, center=true);
    }
  }
}

centerStayDown = 70;
centerStayHeight = innerHeight;
module centerStay()
{
  translate([-centerStayDown,0,centerStayHeight/2-outerHeight/2])
  {
    cylinder(h=centerStayHeight, r=stayOuterRadius, center=true);
  }
}

module centerStayNegative()
{
  translate([-centerStayDown,0,centerStayHeight/2-outerHeight/2-small/2])
  {
    cylinder(h=centerStayHeight+small, r=stayInnerRadius, center=true);
  }
}



// can only fit 1/2 on printer bed:
maxHeight = 40;
difference()
{
  union() {
    base();
    top();
    stay();
    vert();
    centerStay();
  }
  translate([-outerRadius/2-small/2,-outerRadius/2-small/2,0])
    cube([outerRadius+small,outerRadius+small,maxHeight+small], center=true);
  centerStayNegative();
}
