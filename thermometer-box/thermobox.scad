small = 0.1;
wallThickness = 2.0;

innerWidth = 90.0;
innerLength = 163.0;
innerHeight = 36.0;

outerWidth = innerWidth + 2 * wallThickness;
outerLength = innerLength + 2 * wallThickness;
outerHeight = innerHeight + 2 * wallThickness;

outerSize = [outerLength, outerWidth, outerHeight];
innerSize = [innerLength, innerWidth, outerHeight];

latchRadius = 6.0;
latchDownset = 15.0;  // to CENTER of circular latch

cornerRadius = 10.0;

latchNotch = 1.0;

lipAllowance = 0.5;
lipInset = wallThickness + lipAllowance;
lipHeight = 10.0;
lipThickness = 1.0;

lidAllowance = 0.25;  // latch closure length
latchAllowance = 0.25; // latch closure opening

module boxCornerNegative(centre, size, rotation)
{
  translate(centre) {
    rotate([0,0,rotation])
    {
      difference()
      {
        translate([-small,-small,-small/2]) {
          cube([size[0]+small,size[1]+small,size[2]+small]);
        }
        translate([size[0],size[1],-small]) {
          cylinder(h=size[2]+small*2,r=size[0], center=false);
        }
      }
    }
  }
}
module roundedCube(size, radius)
{
  difference() 
  {
    cube(size);
    boxCornerNegative([0,0,0], [radius, radius, size[2]], 0);
    boxCornerNegative([0,size[1],0], [radius, radius, size[2]], 270);
    boxCornerNegative([size[0],0,0], [radius, radius, size[2]], 90);
    boxCornerNegative([size[0],size[1],0], [radius, radius, size[2]], 180);
  }
}

module base()
{
  difference() 
  {
    roundedCube([outerLength,outerWidth,wallThickness+innerHeight], cornerRadius);
    translate([wallThickness,wallThickness,wallThickness]) 
    {
        roundedCube([innerLength,innerWidth,innerHeight+small], cornerRadius-wallThickness);
    }
    latchHoles();
  }
}


module latchHole()
{
  translate([-small,outerWidth/2,outerHeight-latchDownset])
    rotate([0,90,0])
      cylinder(r=latchRadius,h=wallThickness+small*2);
}

module latchHoles()
{
  latchHole();
  translate([outerLength/2,outerWidth/2,0])
    rotate([0,0,180])
      translate([-outerLength/2,-outerWidth/2,0])
        latchHole();
}

module latch()
{
  translate([0,outerWidth/2,0])
  {
    translate([lipInset,-latchRadius,-latchDownset-lidAllowance])
      cube([lipThickness, latchRadius*2,latchDownset+lidAllowance]);
    translate([0,0,-latchDownset-lidAllowance])
      rotate([0,90,0])
        cylinder(r=latchRadius-latchAllowance, h=wallThickness+lipThickness+lipAllowance);
  }
}

module latches()
{
  latch();
  translate([outerLength/2,outerWidth/2,0])
    rotate([0,0,180])
      translate([-outerLength/2,-outerWidth/2,0])
        latch();
}

module latchNotch()
{
  translate([lipInset-small,outerWidth/2,-lipHeight]) 
  {
    translate([0,latchRadius,0])
      cube([lipThickness+small*2,latchNotch, lipHeight]);
    translate([0,-latchRadius-latchNotch,0])
      cube([lipThickness+small*2,latchNotch, lipHeight]);
  }
}

module latchNotches()
{
  latchNotch();
  translate([outerLength/2,outerWidth/2,0])
    rotate([0,0,180])
      translate([-outerLength/2,-outerWidth/2,0])
        latchNotch();
}

module lid()
{
  roundedCube([outerLength, outerWidth, wallThickness], cornerRadius);
  translate([lipInset,lipInset,-lipHeight]) 
  {
    difference()
    {
      roundedCube([outerLength-lipInset*2, outerWidth-lipInset*2, lipHeight], cornerRadius);
      translate([lipThickness,lipThickness,-small])
        roundedCube([outerLength-lipInset*2-lipThickness*2, outerWidth-lipInset*2-lipThickness*2, lipHeight+small], cornerRadius-lipThickness);
    }
  }
}

module latchLid()
{
  latches();
  difference()
  {
    lid();
    latchNotches();
  }
}

base();

// lid open inline for assembly: 
// translate([0,0,outerHeight+20]) latchLid();

// lid for printing

//rotate([180,0,0])
//  translate([0,10,-wallThickness])
//    latchLid();

