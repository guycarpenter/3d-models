/*
 * Wedge is part of the tripod leg clamping mechanism on a cheap
 * tripod we have.  They fall out occaisionally and this is a replacement.
 * There are actually two sizes, and this was modelled from the smaller
 * used on the lower clamps.  However they seem to be a bit large for the
 * lower clamp but still work, and a good size for the upper clamp.
 * I printed them in PLA at 100% infill, no raft, no support.
 */

cylinderR = 16/2.0;
cylinderPos = [0,0,3.48+cylinderR];

lowerWedgeWidth = 6.23;
lowerWedgeLength = 14.47;
lowerWedgeHeight = 3.88;

module lowerWedge()
{
  difference()
  {
    translate([0,0,lowerWedgeHeight/2]) cube([lowerWedgeLength,lowerWedgeWidth,lowerWedgeHeight], center=true);
    translate(cylinderPos) 
    {
      translate([-50,0,0]) rotate([0,90,0]) cylinder(r=cylinderR, h=100, $fn=120);
    }
  }
}

lowerWedge();
