
ringInnerR = 22.5 / 2.0;
ringOuterR = 29.0 / 2.0;
ringHeight = 22.0;
ringSleeve = 11.0;
postR = 43.5;
plateSide = postR + ringSleeve * 2;
small = 0.1;
ringSlot = 2.0;
ringSplineR = 2.1 /2.0;
ringSplineD = 5.1;
hipHeight = ringHeight-ringSleeve;
hipThickness = 3.2;
hipOuterR = ringOuterR + hipThickness;

$fn = 64;

//    /|
//   / |
//  /  |
//  ---+
// H = ringOuterR
// O = ringSplineD/2.0
// sin(a) = O/H
// a = asin(O/H)

ringSplineA = 2 * asin(ringSplineD/2.0 / ringOuterR);
plateSize = [plateSide,plateSide,6];
platePos = [-ringSleeve,-plateSize[1]/2,0];

ringPos = [0,0,32];

hipBaseSize = [hipHeight*2, 58, small];
hipBasePos  = [platePos[0], -hipBaseSize[1]/2, plateSize[2]];

hipMidSize  = [hipHeight, 48, small];
hipMidPos   = [ringPos[0]-ringSleeve, -hipMidSize[1]/2, ringPos[2]-ringOuterR-ringSplineR];

hipTopSize = [hipHeight, 32, small];
hipTopPos = [ringPos[0]-ringSleeve, -hipTopSize[1]/2, ringPos[2]];


module boxCornerNegative(centre, size, rotation)
{
  translate(centre) {
    rotate([0,0,rotation])
    {
      difference()
      {
        translate([-small,-small,-small/2]) {
          cube([size[0]+small,size[1]+small,size[2]+small]);
        }
        translate([size[0],size[1],-small]) {
          cylinder(h=size[2]+small*2,r=size[0], center=false);
        }
      }
    }
  }
}

module ringSpline()
{
  translate([-ringOuterR,0,0])
  cylinder(r=ringSplineR, h=ringSleeve);
}

module ring()
{
  translate([ringPos[0]-ringHeight,ringPos[1],ringPos[2]]) {
    rotate([0,90,0]) {
      rotate([0,0,180]) {
        difference() {
          cylinder(r=ringOuterR, h=ringHeight);
          translate([0,0,-small]) 
          {
            cylinder(r=ringInnerR, h=ringHeight+small*2);
            cube([ringOuterR+small,ringSlot,ringHeight+small*2]);
          }
        }
        rotate([0,0,0]) ringSpline();
        rotate([0,0,0+ringSplineA]) ringSpline();
        rotate([0,0,0-ringSplineA]) ringSpline();
      }
    }
  }
}

module hipMask()
{
  translate([-ringSleeve,-50,plateSize[2]]) {
    cube([ringSleeve,100,100]);
  }
}

module hip()
{
  hull()
  {
    translate(hipBasePos) cube(hipBaseSize);
    translate(hipMidPos) cube(hipMidSize);
  } 
  difference()
  {
    hull()
    {
      translate(hipMidPos) cube(hipMidSize);
      translate(hipTopPos) cube(hipTopSize);
    }
    translate([-small,0,ringPos[2]])
    {
      rotate([0,90,0]) 
      {
        translate([0,0,-50]) {
          cylinder(r=ringOuterR, 100);
	}
      }
    }
  }
}

module hipA()
{
  intersection()
  {
    hip();
    hipMask();
  }
}

module hipB()
{
  difference()
  {
    hip();
    hipMask();
  }
}

holeR1 = 21.0/2;

module plateHole()
{
  translate([plateSize[0]/2,plateSize[1]/2,-small]) 
  {
    cylinder(h=plateSize[2]+small*2, r=6.0/2.0);
    //cylinder(h=1.5+small, r=21.0/2);
    //translate([0,0,1.5]) 
    //{
    //  cylinder(h=plateSize[2]-3.0+small, r1=21.0/2, r2=21.0/2-plateSize[2]+3);
    //}
  }
}

plateCornerR = 5.0;
plateCornerSize = [plateCornerR, plateCornerR, plateSize[2]];

module plate()
{
  translate(platePos)
  {
    difference() {
      cube(plateSize);
      plateHole();
      boxCornerNegative([0,0,0], plateCornerSize,0);
      boxCornerNegative([0,plateSize[1],0], plateCornerSize,270);
      boxCornerNegative([plateSize[0],0,0], plateCornerSize,90);
      boxCornerNegative([plateSize[0],plateSize[1],0], plateCornerSize,180);
   }
  }
}

module platePart()
{
  plate();
  hipB();
}

module ringPart()
{
  ring();
  hipA();
}

platePart();
ringPart();
