// 68mm diam
// 4.5 mm depth

holeDiam    = 68.0;
holeDepth   =  4.5;

small     =     1.0;  // used to prevent incident faces
sleeveDepth =   2.5;
sleeveDiam  =   holeDiam - 0.5;
sleeveWall  =   2.0;

capDepth = 2.0;
capDiam  = sleeveDiam + 5.0;

clipHeight = 10.0;
clipDepth  =  2.0;
clipWidth  =  5.0;
clipProj   =  2.0;
module clip()
{
  translate([sleeveDiam/2-clipDepth,-clipWidth/2,0]) {
    cube(size=[clipDepth,clipWidth, clipHeight+holeDepth]);
    translate([clipDepth,0,holeDepth]) {
      polyhedron ( 
        points = [
            [0, 0, clipHeight],
            [0, clipWidth, clipHeight],
            [0, clipWidth, 0],
            [0, 0, 0],
            [clipProj, 0, clipHeight/2],
            [clipProj, clipWidth, clipHeight/2]
        ], 
        triangles = [
            [0,3,2],
            [0,2,1],
            [3,0,4],
            [1,2,5],
            [0,5,4],
            [0,1,5],
            [5,2,4],
            [4,2,3]
        ]
      );
    }
  }
}
  
module sleeve()
{
  translate([0,0,0]) {
    difference() {
      cylinder(h=sleeveDepth, r=sleeveDiam/2, $fa=1);
      translate([0,0,-small]) {
        cylinder(h=sleeveDepth+small*2, r=sleeveDiam/2-sleeveWall, $fa=1);
      }
    }
  }
}

module cap()
{
  translate([0,0,-capDepth]) {
    cylinder(h=capDepth, r=capDiam/2, $fa=1);
  }
}

sleeve();
cap();
clip();
rotate(a= 90, v=[0,0,1]) clip();
rotate(a=180, v=[0,0,1]) clip();
rotate(a=270, v=[0,0,1]) clip();

  
