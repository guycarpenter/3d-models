small = 0.1;

pegH = 7.0;
pegHoleR = 1.0;
pegR = 3.0;
pegFlangeH = 2.0;
pegFlangeR = 4.0;
pegSinkR = 2.0;
pegSinkH = 1.0;

$fs = 1;
$fa = 1;

module peg_negative()
{
    translate([0,0,-small/2]) 
    {
      cylinder(h=pegH+small, r=pegHoleR);
      cylinder(h=pegSinkH+small, r=pegSinkR);
    }
}

module peg_positive()
{
    cylinder(h=pegH, r=pegR);
    cylinder(h=pegFlangeH, r=pegFlangeR);
}

module peg()
{
  difference()
  {
    peg_positive();
    peg_negative();
  }        
}

socketW = 28;
socketH = 28;
socketD = 7;
flapW = 10;
flapD = 2.5;

slotY0 = 5;
slotY1 = -5;
recessR = pegFlangeR+1.0;
slotR = pegR+0.5;
recessD = pegFlangeH + 0.5;
holeR = pegFlangeR+2.0;

module socket_negative()
{
  // open space on top
  
  hull() 
  {
    translate([0,slotY0,socketD-recessD+small]) {
      cylinder(h=recessD+small, r=recessR);
    }
    translate([0,slotY1,socketD-recessD+small]) {
      cylinder(h=recessD+small, r=recessR);
    }
  }

  // full-size hole
  translate([0,slotY0,-small/2]) 
  {
    cylinder(h=socketD+small, r=holeR);
  }

  // slot
  hull()
  {
    translate([0,slotY0,-small/2]) 
    {
      cylinder(h=socketD+small, r=slotR);
    }
    translate([0,slotY1,-small/2]) 
    {
      cylinder(h=socketD+small, r=slotR);
    }
  }

}

module flap_negative()
{
  // clip corner
  clipW = socketW+flapD;
  f = .87;
  translate([-socketW*f,-socketH*f,socketD/2]) {
    rotate([0,0,45]) {
      cube([clipW, clipW, socketD+small],center=true);
    }
  }

  screw0X = -2;
  screw1X = 8;
  screwR = 1.5;

  // screw holes
  translate([screw0X,-socketH/2-flapW/2,-small/2]) {
    cylinder(h=flapD+small, r=screwR);
  }
  translate([screw1X,-socketH/2-flapW/2,-small/2]) {
    cylinder(h=flapD+small, r=screwR);
  }
  translate([-socketH/2-flapW/2,screw0X,-small/2]) {
    cylinder(h=flapD+small, r=screwR);
  }
  translate([-socketH/2-flapW/2,screw1X,-small/2]) {
    cylinder(h=flapD+small, r=screwR);
  }
  

}

module socket_positive()
{
  translate([0,0,socketD/2]) 
  {
    cube(size=[socketW,socketH,socketD], center=true);
  }
  translate([-socketW/2-flapW/2,0,flapD/2]) 
  {
    cube(size=[flapW, socketH, flapD], center=true);
  }
  translate([0,-socketH/2-flapW/2,flapD/2])
  {
    cube(size=[socketW, flapW, flapD], center=true);
  }
}

module peg_inplace()
{
  translate([0,0,pegH]) 
  {
    mirror([0,0,1]) 
    {
      peg();
    }
  }
}

socketRotation = 0;

module socket()
{
  difference()
  {
    socket_positive();
    rotate([0,0,socketRotation])
    {
      socket_negative();
    }
    flap_negative();
  }        
}


socket();
%peg_inplace();
