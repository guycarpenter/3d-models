outer_rad      = 12.8 / 2.0;
wall_thickness =  0.7;
height         = 60.0;
small          =  0.1;

$fa = 1;
$fs = .5;

difference()
{
  cylinder(h=height, r=outer_rad);
  translate([0,0,-small])
  {
    cylinder(h=height+2*small, r=outer_rad-wall_thickness);
  }
}
