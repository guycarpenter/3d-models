small = 0.1;
$fa = 1.0;
$fs = 0.5;

tube_outer_rad = 12.8 / 2.0;
tube_inner_rad = tube_outer_rad - 1.0;
tube_height =    20.0;
tube_offset =     8.0;

flute_height = 10.0;
flute_radius = 10.0;
flute_arc_r1 = 10.0 + small;
flute_arc_r2 = 10.0;
flute_cap_height = 2;

module flute_negative()
{
  translate([0,0,flute_cap_height+flute_arc_r2])
  {
    rotate_extrude(convexity = 10)
    {
      translate([flute_arc_r1, 0, 0])
      {
        circle(r = flute_arc_r2);
      }
    }
  }
}

module flute_positive()
{
  translate([0,0,0])
  {
    cylinder(h=flute_height, r=flute_radius);
  }        
}

module flute()
{
  difference()
  {
    flute_positive();
    flute_negative();
  }
}

brace_wall = 0.7;
brace_height = 20.0;
brace_diam   = tube_outer_rad * 2 - 0.5;
brace_offset = flute_cap_height;
module brace()
{
  translate([-brace_wall/2,-brace_diam/2,brace_offset])
  {
    cube([brace_wall, brace_diam, brace_height]);
  }
}

module braces()
{
  difference() {
    union() 
    {
      brace();
      rotate([0,0,90]) 
      {
        brace();
      }
    }
    tube();
  } 
}

module tube()
{
  translate([0,0,tube_offset])
  {
    difference()
    {
      cylinder(h=tube_height, r=tube_outer_rad);
      translate([0,0,-small])
      {
        cylinder(h=tube_height+2*small, r=tube_inner_rad);
      }
    }
  }
}

//tube();
braces();
flute();

