inner_rad      =  8.4 / 2;
wall_thickness =  0.7;
outer_rad      = inner_rad + wall_thickness;
height         = 60.0;
small          =  0.1;

$fa = 1;
$fs = .5;

difference()
{
  cylinder(h=height, r=outer_rad);
  translate([0,0,-small])
  {
    cylinder(h=height+2*small, r=inner_rad);
  }
}
